from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #


class project_task_tambahan_propose_rejected(osv.Model):
    _name = 'project.tambahan.kreatifitas.propose.rejected'
    _description = 'Realisasi Tugas Tambahan Dan Kreatifitas Ditolak Atasan'
    _columns = {
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'notes'      : fields.text('Catatan Koreksi',required=True),
        'task_id'     : fields.many2one('project.tambahan.kreatifitas', 'Realisasi', readonly=True),
      
       'tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
        'uraian_tugas_tambahan'     : fields.text('Uraian Tugas Tambahan'),
        'rl_presiden_tugas_tambahan'     : fields.boolean('Presiden'),
        'rl_gubernur_tugas_tambahan'     : fields.boolean('Gubernur'),
        'rl_opd_tugas_tambahan'     : fields.boolean('Kepala DITJEN'),
        'attach_tugas_tambahan'              : fields.binary('SK/SP/ST'),
        'nilai_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
        'uraian_kreatifitas'     : fields.text('Uraian Kreatifitas'),
        'attach_kreatifitas'              : fields.binary('Bukti Pengakuan'),
        'tupoksi_kreatifitas': fields.boolean('Tupoksi'),
        'rl_presiden_kreatifitas'     : fields.boolean('Presiden'),
        'rl_gubernur_kreatifitas'     : fields.boolean('Gubernur'),
        'rl_opd_kreatifitas'     : fields.boolean('Kepala DITJEN'),
       
    }
    def action_tambahan_propose_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.tambahan.kreatifitas')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({             
                                 'suggest_tugas_tambahan'              : task_obj.tugas_tambahan,
                                 'suggest_uraian_tugas_tambahan'       : task_obj.uraian_tugas_tambahan,
                                'suggest_rl_opd_tugas_tambahan'       : task_obj.rl_opd_tugas_tambahan,
                                'suggest_rl_gubernur_tugas_tambahan'  : task_obj.rl_gubernur_tugas_tambahan,
                                'suggest_rl_presiden_tugas_tambahan'  : task_obj.rl_presiden_tugas_tambahan,
                                'suggest_nilai_kreatifitas'           : task_obj.nilai_kreatifitas,
                                'suggest_uraian_kreatifitas'          : task_obj.uraian_kreatifitas,
                                'suggest_tupoksi_kreatifitas'         : task_obj.tupoksi_kreatifitas,
                                'suggest_rl_opd_kreatifitas'          : task_obj.rl_opd_kreatifitas,
                                'suggest_rl_gubernur_kreatifitas'     : task_obj.rl_gubernur_kreatifitas,
                                'suggest_rl_presiden_kreatifitas'     : task_obj.rl_presiden_kreatifitas,
                                
                                'is_suggest' : True,
                                'notes_atasan' : task_obj.notes,
                                'state':'rejected_manager',
                                })               
                # end if           
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        
    
project_task_tambahan_propose_rejected()

class project_task_tambahan_appeal_rejected(osv.Model):
    _name = 'project.tambahan.kreatifitas.appeal.rejected'
    _description = 'Realisasi Tugas Tambahan Dan Kreatifitas Banding Ditolak'
    _columns = {
        'is_appeal': fields.boolean('Tambahkan Koreski Penilaian'),
        'notes'      : fields.text('Catatan Koreksi',required=True),
        'task_id'     : fields.many2one('project.tambahan.kreatifitas', 'Realisasi', readonly=True),
        'tugas_tambahan'     : fields.integer('Jumlah Tugas Tambahan'),
        'uraian_tugas_tambahan'     : fields.text('Uraian Tugas Tambahan'),
        'rl_presiden_tugas_tambahan'     : fields.boolean('Presiden'),
        'rl_gubernur_tugas_tambahan'     : fields.boolean('Gubernur'),
        'rl_opd_tugas_tambahan'     : fields.boolean('Kepala DITJEN'),
        'attach_tugas_tambahan'              : fields.binary('SK/SP/ST'),
        'nilai_kreatifitas'     : fields.integer('Jumlah Kreatifitas'),
        'uraian_kreatifitas'     : fields.text('Uraian Kreatifitas'),
        'attach_kreatifitas'              : fields.binary('Bukti Pengakuan'),
        'tupoksi_kreatifitas': fields.boolean('Tupoksi'),
        'rl_presiden_kreatifitas'     : fields.boolean('Presiden'),
        'rl_gubernur_kreatifitas'     : fields.boolean('Gubernur'),
        'rl_opd_kreatifitas'     : fields.boolean('Kepala DITJEN'),
       
    }
    def action_tambahan_appeal_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.tambahan.kreatifitas')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({             
                                 'appeal_tugas_tambahan'              : task_obj.tugas_tambahan,
                                 'appeal_uraian_tugas_tambahan'       : task_obj.uraian_tugas_tambahan,
                                'appeal_rl_opd_tugas_tambahan'       : task_obj.rl_opd_tugas_tambahan,
                                'appeal_rl_gubernur_tugas_tambahan'  : task_obj.rl_gubernur_tugas_tambahan,
                                'appeal_rl_presiden_tugas_tambahan'  : task_obj.rl_presiden_tugas_tambahan,
                                'appeal_nilai_kreatifitas'           : task_obj.nilai_kreatifitas,
                                'appeal_uraian_kreatifitas'          : task_obj.uraian_kreatifitas,
                                'appeal_tupoksi_kreatifitas'         : task_obj.tupoksi_kreatifitas,
                                'appeal_rl_opd_kreatifitas'          : task_obj.rl_opd_kreatifitas,
                                'appeal_rl_gubernur_kreatifitas'     : task_obj.rl_gubernur_kreatifitas,
                                'appeal_rl_presiden_kreatifitas'     : task_obj.rl_presiden_kreatifitas,
                                
                                'is_appeal' : True,
                                'notes_atasan_banding' : task_obj.notes,
                                
                                })               
                # end if           
        
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        task_pool.fill_task_automatically_with_appeal(cr, uid, [task_obj.task_id.id], context);
        task_pool.write(cr, uid, [task_obj.task_id.id],  {'state': 'evaluated'}, context)
        
    
project_task_tambahan_appeal_rejected()


class project_tambahan_kreatifitas_verificate_rejected(osv.Model):
    _name = 'project.tambahan.kreatifitas.verificate.rejected'
    _description = 'Realisasi Tugas Tambahan Dan Kreatifitas, Verifikasi Ditolak'
    _columns = {
        'is_control': fields.boolean('Tambahkan Koreski Verifikasi'),
        'notes'      : fields.text('Kesimpulan Catatan Koreksi',required=True),
        'control_count': fields.integer('Jumlah Koreksi Verifikasi', readonly=True),
        'task_id'     : fields.many2one('project.tambahan.kreatifitas', 'Realisasi', readonly=True),
       
    }
    def action_verificate_rejected(self, cr, uid, ids, context=None):
        """  Pengajuan Di tolak
        """
        vals = {}
        task_pool = self.pool.get('project.tambahan.kreatifitas')
        task_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({               'control_count'     : task_obj.control_count,
                                    'is_control' : True,
                                    'is_suggest' : True,
                                    'notes_bkd' : task_obj.notes,
                                    'state':'rejected_bkd',
                                     })
        task_pool.write(cr, uid, [task_obj.task_id.id], vals, context)
        
    
project_tambahan_kreatifitas_verificate_rejected()