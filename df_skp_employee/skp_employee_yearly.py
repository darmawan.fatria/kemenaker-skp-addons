# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import fields, osv
from openerp import SUPERUSER_ID
from datetime import datetime,timedelta
import time
from mx import DateTime



class skp_employee_yearly(osv.Model):
    _name = 'skp.employee.yearly'
    _description = 'Rekapitulasi Tahunan'
    def _get_total_perilaku(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        
        for me_obj in self.browse(cr, uid, ids, context=context):
            total = me_obj.nilai_pelayanan+ me_obj.nilai_integritas + me_obj.nilai_komitmen + me_obj.nilai_disiplin +me_obj.nilai_kepemimpinan + me_obj.nilai_kerjasama
            res[me_obj.id] = total   
        return res


    def _hmm_get_akumulasi_realiasai_per_bulan(self, cr, user_id, employee_id,target_period_year, context=None):


        if not employee_id and not target_period_year :
            return False
        uid = SUPERUSER_ID
        skp_employee_pool = self.pool.get('skp.employee')
        project_pool = self.pool.get('project.project')
        partner_pool = self.pool.get('res.partner')
        data = {}
        data['jml_skp_done']=0
        data['jml_skp']=0
        data['jml_realisasi_skp']=0
        data['nilai_skp']=0
        data['nilai_skp_percent']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['nilai_skp_tambahan_percent']=0
        data['nilai_skp_tambahan']=0
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_total']=0
        data['indeks_nilai_total']=''

        skp_employee_ids = skp_employee_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        jml_skp=jml_realisasi_skp=count_tambahan_kreatifitas=count=0
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        fn_nilai_tambahan=fn_nilai_kreatifitas=nilai_total=0
        haveRefData=False;
        for skp_obj in  skp_employee_pool.read(cr, uid, skp_employee_ids, ['id','jml_perilaku','nilai_perilaku','nilai_perilaku_percent'
                                                                           ,'nilai_pelayanan','nilai_integritas','nilai_komitmen'
                                                                           ,'nilai_disiplin','nilai_kerjasama','nilai_kepemimpinan'
                                                                           ,'fn_nilai_tambahan','fn_nilai_kreatifitas','jml_all_skp'
                                                                           ,'jml_skp','nilai_skp','nilai_total'
                                                                           ,'employee_id'
                                                                           ], context=context) :

            if not skp_obj:
                return;
            else:
                haveRefData=True;

            jml_perilaku+=skp_obj['jml_perilaku']
            nilai_perilaku+=skp_obj['nilai_perilaku']
            nilai_perilaku_percent+=skp_obj['nilai_perilaku_percent']
            nilai_pelayanan+=skp_obj['nilai_pelayanan']
            nilai_integritas+=skp_obj['nilai_integritas']
            nilai_komitmen+=skp_obj['nilai_komitmen']
            nilai_disiplin+=skp_obj['nilai_disiplin']
            nilai_kerjasama+=skp_obj['nilai_kerjasama']
            nilai_kepemimpinan+=skp_obj['nilai_kepemimpinan']

            fn_nilai_tambahan+=skp_obj['fn_nilai_tambahan']
            fn_nilai_kreatifitas+=skp_obj['fn_nilai_kreatifitas']

            jml_realisasi_skp+=skp_obj['jml_all_skp']
            jml_skp+=skp_obj['jml_skp']
            nilai_skp+=skp_obj['nilai_skp']
            nilai_total+=skp_obj['nilai_total']

            if fn_nilai_tambahan or fn_nilai_kreatifitas :
                if fn_nilai_tambahan > 0 or fn_nilai_kreatifitas > 0:
                    count_tambahan_kreatifitas+=1

            if skp_obj['nilai_total'] and skp_obj['nilai_total'] >0 :
                count+=1
        if haveRefData and target_period_year:
            data['jml_perilaku']=jml_perilaku
            data['nilai_perilaku']=nilai_perilaku
            data['nilai_perilaku_percent']=nilai_perilaku_percent
            data['nilai_pelayanan']=nilai_pelayanan
            data['nilai_integritas']=nilai_integritas
            data['nilai_komitmen']=nilai_komitmen
            data['nilai_disiplin']=nilai_disiplin
            data['nilai_kerjasama']=nilai_kerjasama
            data['nilai_kepemimpinan']=nilai_kepemimpinan
            if count_tambahan_kreatifitas > 0 :
                data['fn_nilai_tambahan']=fn_nilai_tambahan/count_tambahan_kreatifitas
                data['fn_nilai_kreatifitas']=fn_nilai_kreatifitas/count_tambahan_kreatifitas
            data['count_of_month']=count

            jml_perilaku=12
            if haveRefData and jml_perilaku >0 :
                data['nilai_perilaku']=nilai_perilaku/jml_perilaku
                data['nilai_perilaku_percent']=nilai_perilaku_percent/jml_perilaku
                data['nilai_pelayanan']=nilai_pelayanan/jml_perilaku
                data['nilai_integritas']=nilai_integritas/jml_perilaku
                data['nilai_komitmen']=nilai_komitmen/jml_perilaku
                data['nilai_disiplin']=nilai_disiplin/jml_perilaku
                data['nilai_kerjasama']=nilai_kerjasama/jml_perilaku

                data['nilai_kepemimpinan'] =0.0
                data['indeks_nilai_kepemimpinan']=''
                if skp_obj['employee_id']:
                    job_type=''
                    for employee_id_obj in partner_pool.read(cr, uid, [skp_obj['employee_id'][0]], ['id','job_type',], context=context) :
                        job_type = employee_id_obj['job_type']

                    if job_type == 'struktural':
                        data['nilai_kepemimpinan']=nilai_kepemimpinan/jml_perilaku
                        data['indeks_nilai_kepemimpinan']=self.get_indeks_nilai(cr, uid, data['nilai_kepemimpinan'], context)



                data['indeks_nilai_pelayanan']=self.get_indeks_nilai(cr, uid, data['nilai_pelayanan'], context)
                data['indeks_nilai_integritas']=self.get_indeks_nilai(cr, uid, data['nilai_integritas'], context)
                data['indeks_nilai_komitmen']=self.get_indeks_nilai(cr, uid, data['nilai_komitmen'], context)
                data['indeks_nilai_disiplin']=self.get_indeks_nilai(cr, uid, data['nilai_disiplin'], context)
                data['indeks_nilai_kerjasama']=self.get_indeks_nilai(cr, uid, data['nilai_kerjasama'], context)
        #SKP=-----------------


        if haveRefData and nilai_skp:
                 data['jml_skp']=jml_skp
                 data['jml_skp_done']=jml_skp
                 data['jml_realisasi_skp']=jml_realisasi_skp
                 data['nilai_skp']=nilai_skp/12
                 data['nilai_skp_percent']=data['nilai_skp']*0.6
                 data['nilai_skp_tambahan']=data['nilai_skp']
                 data['nilai_skp_tambahan_percent']=data['nilai_skp_percent']


        if haveRefData and count_tambahan_kreatifitas > 0 :
                data['fn_nilai_tambahan']=fn_nilai_tambahan
                data['fn_nilai_kreatifitas']=fn_nilai_kreatifitas
                data['nilai_skp_tambahan']=data['nilai_skp']+fn_nilai_tambahan+fn_nilai_kreatifitas
                data['nilai_skp_tambahan_percent']=data['nilai_skp_percent']+data['fn_nilai_tambahan']+data['fn_nilai_kreatifitas']
        else :
                data['fn_nilai_tambahan']=0
                data['fn_nilai_kreatifitas']=0

        if count>0 :
                data['nilai_total']=data['nilai_skp_tambahan_percent']+data['nilai_perilaku_percent']
                #print "nilai total vs nilai/12 = ",data['nilai_total']," vs ",(nilai_total/12)
                data['indeks_nilai_total']=self.get_indeks_nilai(cr, uid, data['nilai_total'], context)



        return data


    
        
    _columns = {
        'target_period_year'     : fields.char('Periode Tahun',size=4, required=True),
        'employee_id': fields.many2one('res.partner', 'Pegawai Yang Dinilai', readonly=True),
        'user_id': fields.many2one('res.users','User Login', readonly=True ),
        'company_id': fields.many2one('res.company', 'DITJEN', readonly=True),
        'employee_company_id': fields.related('employee_id', 'company_id', relation='res.company', type='many2one', string='DITJEN', store=False),
        'user_id_bkd': fields.related('employee_company_id', 'user_id_bkd',  relation='res.users', type='many2one', string='Verifikatur', store=False),
        'biro_id': fields.many2one('partner.employee.biro', 'Bagian / Bidang',readonly=True ),
        'department_id': fields.many2one('partner.employee.department', 'Unit Kerja',readonly=True),
        'golongan_id': fields.many2one('partner.employee.golongan', 'Golongan',readonly=True), 
        'job_id': fields.many2one('partner.employee.job', 'Jabatan',readonly=True),
        'jml_skp_done': fields.integer(string='Jumlah Target SKP Yang Sudah Dinilai',  help="Jumlah SKP",readonly = True),
        'jml_skp': fields.integer(string='Jumlah Target SKP',  help="Jumlah SKP",readonly = True),
        'jml_realisasi_skp': fields.integer(string='Jumlah Realisasi SKP',  help="Jumlah SKP",readonly = True),
        'jml_perilaku': fields.integer( string='Jumlah Realisasi Perilaku',  help="Jumlah SKP",readonly = True),
        'nilai_skp': fields.float( string='Nilai SKP',  help="Nilai SKP Akan Muncul Apabila Semua Kegiatan Dibulan Tertentu Sudah Selesai Dinilai", readonly = True),
        'nilai_skp_tambahan': fields.float( string='Nilai SKP+Tambahan+Kreatifitas',  help="Nilai SKP Akan Muncul Apabila Semua Kegiatan Dibulan Tertentu Sudah Selesai Dinilai", readonly = True),
        'nilai_skp_percent': fields.float( string='Nilai SKP(%)',  help="60% Dari Kegiatan DPA Biro, APBN, SOTK", readonly = True),
        'nilai_skp_tambahan_percent': fields.float( string='Nilai SKP+TB+Kreatifitas (%)',  help="60% Dari Kegiatan DPA Biro, APBN, SOTK Ditambah rata2 Tugas Tambahan Dan Nilai Kreatifitas", readonly = True),
        'nilai_perilaku': fields.float( string='Nilai Perilaku', help="40% Kontribusi untuk nilai perilaku", readonly = True),
        'nilai_perilaku_percent': fields.float(string='Nilai Perilaku(%)',  help="40% Kontribusi untuk nilai perilaku",readonly = True),
        'fn_nilai_tambahan': fields.float( string='Nilai Tambahan',  help="Nilai Tambahan", readonly = True),
        'fn_nilai_kreatifitas': fields.float( string='Nilai Kreatifitas',help="Nilai Kreatifitas",readonly = True),
        'nilai_total': fields.float(string='Nilai Total (%)',  help="60% SKP + maks 2, Nilai Tambahan + 40% perilaku", readonly = True),
        'nilai_tpp': fields.float( string='TPP', help="TPP", readonly = True),
        'nilai_kepemimpinan': fields.float('Nilai Kepemimpinan', readonly=True),
        'nilai_kerjasama': fields.float('Nilai Kerjasama', readonly=True),
        'nilai_integritas': fields.float('Nilai Integritas', readonly=True),
        'nilai_komitmen': fields.float('Nilai Komitmen', readonly=True),
        'nilai_disiplin': fields.float('Nilai Disiplin', readonly=True),
        'nilai_pelayanan': fields.float('Nilai Pelayanan', readonly=True),
        'indeks_nilai_kepemimpinan': fields.char('Indeks Nilai Kepemimpinan', size=25,readonly=True),
        'indeks_nilai_kerjasama': fields.char('Indeks Nilai Kerjasama',  size=25,readonly=True),
        'indeks_nilai_integritas': fields.char('Indeks Nilai Integritas',  size=25,readonly=True),
        'indeks_nilai_komitmen': fields.char('Indeks Nilai Komitmen',  size=25,readonly=True),
        'indeks_nilai_disiplin': fields.char('Indeks Nilai Disiplin',  size=25,readonly=True),
        'indeks_nilai_pelayanan': fields.char('Indeks Nilai Pelayanan',  size=25,readonly=True),
        'indeks_nilai_total': fields.char('Indeks Nilai Total',  size=25,readonly=True),
        'total_perilaku': fields.function(_get_total_perilaku, string='Penambahan Semua Aspek Perilaku', type='integer',
            store = False),
        'jumlah_perhitungan_skp'     : fields.float('Jumlah Perhitungan SKP', readonly=True,),
         'indeks_nilai_skp' :fields.char('Indeks Nilai SKP',size=25)
   }
    def get_indeks_nilai(self,cr,uid,nilai,context=None):
        ret_val=''
        lookup_nilai_pool = self.pool.get('acuan.penilaian')
        lookup_nilai_id = lookup_nilai_pool.search(cr, uid, [('kategori_nilai', '=', 'threshold'), ('active', '=', True), ('type', '=', 'lain_lain')
                                                            , ('code', 'in', ('index_nilai_a','index_nilai_b','index_nilai_c','index_nilai_d','index_nilai_e'))
                                                            ,('nilai_bawah', '<=', nilai), ('nilai_atas', '>=', nilai)], context=None)
        if lookup_nilai_id :
            lookup_nilai_obj = lookup_nilai_pool.browse(cr, uid, lookup_nilai_id, context=None)[0]
            ret_val = lookup_nilai_obj.name
        
        return ret_val
    def do_yearly_calculation(self, cr, uid, ids,  context=None):
        skp_employee_pool = self.pool.get('skp.employee')
        for yearly_obj in self.browse(cr, uid, ids, context=context):
                            data_skp_summary  = self._get_akumulasi_realisasi_skp_tambahan_per_bulan(cr,  yearly_obj.user_id,yearly_obj.employee_id, yearly_obj.target_period_year, context=context)
                            data_perilaku_summary  = self._get_akumulasi_realisasi_perilaku_per_bulan(cr, yearly_obj.user_id,yearly_obj.employee_id, yearly_obj.target_period_year, context=context)


                            if data_skp_summary and data_perilaku_summary :
                                nilai_total = round(data_skp_summary['nilai_skp_tambahan_percent'] + data_perilaku_summary['nilai_perilaku_percent'],2)
                                indeks_nilai_total = self.get_indeks_nilai(cr,uid,nilai_total,context=None)
                                update_values = {
                                    'jml_skp': data_skp_summary['jml_skp'],
                                    'jml_skp_done': data_skp_summary['jml_skp_done'],
                                    'jml_realisasi_skp':data_skp_summary['jml_realisasi_skp'],
                                    'nilai_skp': data_skp_summary['nilai_skp']   ,
                                    'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                    'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],
                                    'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],


                                    'fn_nilai_tambahan': data_skp_summary['fn_nilai_tambahan'],
                                    'fn_nilai_kreatifitas': data_skp_summary['fn_nilai_kreatifitas'],

                                    'jml_perilaku': data_perilaku_summary['jml_perilaku'],
                                    'nilai_perilaku': data_perilaku_summary['nilai_perilaku'],
                                    'nilai_perilaku_percent': data_perilaku_summary['nilai_perilaku_percent'],

                                    'nilai_total': nilai_total,

                                    'nilai_pelayanan': data_perilaku_summary['nilai_pelayanan'],
                                    'nilai_integritas': data_perilaku_summary['nilai_integritas'],
                                    'nilai_komitmen': data_perilaku_summary['nilai_komitmen'],
                                    'nilai_disiplin': data_perilaku_summary['nilai_disiplin'],
                                    'nilai_kerjasama': data_perilaku_summary['nilai_kerjasama'],
                                    'nilai_kepemimpinan': data_perilaku_summary['nilai_kepemimpinan'],
                                    'indeks_nilai_pelayanan': data_perilaku_summary['indeks_nilai_pelayanan'],
                                    'indeks_nilai_integritas': data_perilaku_summary['indeks_nilai_integritas'],
                                    'indeks_nilai_komitmen': data_perilaku_summary['indeks_nilai_komitmen'],
                                    'indeks_nilai_disiplin': data_perilaku_summary['indeks_nilai_disiplin'],
                                    'indeks_nilai_kerjasama': data_perilaku_summary['indeks_nilai_kerjasama'],
                                    'indeks_nilai_kepemimpinan': data_perilaku_summary['indeks_nilai_kepemimpinan'],

                                    'indeks_nilai_total': indeks_nilai_total,
                                }
                                self.write(cr , uid,[yearly_obj.id,], update_values, context=None)
        return True;











    def _get_akumulasi_realiasai_per_bulan_opt(self, cr, user_id, employee_id,target_period_year, context=None):


        if not employee_id and not target_period_year :
            return False

        skp_employee_pool = self.pool.get('skp.employee')
        project_pool = self.pool.get('project.project')
        partner_pool = self.pool.get('res.partner')
        tambahan_pool = self.pool.get('project.tambahan.yearly')
        uid = SUPERUSER_ID
        data = {}
        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_perilaku_percent']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan_percent']=0
        data['nilai_skp']=0
        data['nilai_skp_tambahan']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['nilai_total']=0
        data['indeks_nilai_total']=''
        data['indeks_nilai_skp']=''
        data['jumlah_perhitungan_skp']=0
        skp_employee_ids = skp_employee_pool.search(cr, uid, [('user_id', '=', user_id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        jml_skp=jml_realisasi_skp=count_tambahan_kreatifitas=count=0
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        nilai_total=0
        for skp_obj in  skp_employee_pool.read(cr, uid, skp_employee_ids, ['id','jml_perilaku','nilai_perilaku','nilai_perilaku_percent'
                                                                           ,'nilai_pelayanan','nilai_integritas','nilai_komitmen'
                                                                           ,'nilai_disiplin','nilai_kerjasama','nilai_kepemimpinan'
                                                                           ,'nilai_total'
                                                                           ,'employee_id'
                                                                           ], context=context) :

            #Hitung Perilaku
            jml_perilaku+=skp_obj['jml_perilaku']
            nilai_perilaku+=skp_obj['nilai_perilaku']
            nilai_perilaku_percent+=skp_obj['nilai_perilaku_percent']
            nilai_pelayanan+=skp_obj['nilai_pelayanan']
            nilai_integritas+=skp_obj['nilai_integritas']
            nilai_komitmen+=skp_obj['nilai_komitmen']
            nilai_disiplin+=skp_obj['nilai_disiplin']
            nilai_kerjasama+=skp_obj['nilai_kerjasama']
            nilai_kepemimpinan+=skp_obj['nilai_kepemimpinan']
            count+=1


        if target_period_year:
            data['jml_perilaku']=jml_perilaku #jumlah perilaku yang sd
            data['nilai_perilaku']=nilai_perilaku
            data['nilai_perilaku_percent']=nilai_perilaku_percent
            data['nilai_pelayanan']=nilai_pelayanan
            data['nilai_integritas']=nilai_integritas
            data['nilai_komitmen']=nilai_komitmen
            data['nilai_disiplin']=nilai_disiplin
            data['nilai_kerjasama']=nilai_kerjasama
            data['nilai_kepemimpinan']=nilai_kepemimpinan

            data['count_of_month']=count

            jml_perilaku=12
            if jml_perilaku >0 and count >0  :
                data['nilai_perilaku']=nilai_perilaku/jml_perilaku
                data['nilai_perilaku_percent']=nilai_perilaku_percent/jml_perilaku
                data['nilai_pelayanan']=nilai_pelayanan/jml_perilaku
                data['nilai_integritas']=nilai_integritas/jml_perilaku
                data['nilai_komitmen']=nilai_komitmen/jml_perilaku
                data['nilai_disiplin']=nilai_disiplin/jml_perilaku
                data['nilai_kerjasama']=nilai_kerjasama/jml_perilaku

                data['nilai_kepemimpinan'] =0.0
                data['indeks_nilai_kepemimpinan']=''
                if skp_obj['employee_id']:
                    job_type=''
                    for employee_id_obj in partner_pool.read(cr, uid, [skp_obj['employee_id'][0]], ['id','job_type',], context=context) :
                        job_type = employee_id_obj['job_type']

                    if job_type == 'struktural':
                        data['nilai_kepemimpinan']=nilai_kepemimpinan/jml_perilaku
                        data['indeks_nilai_kepemimpinan']=self.get_indeks_nilai(cr, uid, data['nilai_kepemimpinan'], context)



                data['indeks_nilai_pelayanan']=self.get_indeks_nilai(cr, uid, data['nilai_pelayanan'], context)
                data['indeks_nilai_integritas']=self.get_indeks_nilai(cr, uid, data['nilai_integritas'], context)
                data['indeks_nilai_komitmen']=self.get_indeks_nilai(cr, uid, data['nilai_komitmen'], context)
                data['indeks_nilai_disiplin']=self.get_indeks_nilai(cr, uid, data['nilai_disiplin'], context)
                data['indeks_nilai_kerjasama']=self.get_indeks_nilai(cr, uid, data['nilai_kerjasama'], context)


        #SKP=-----------------
        target_ids = project_pool.search(cr, uid, [('user_id', '=', user_id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', 'in' , ('confirm','closed'))
                                                   ], context=None)
        count_skp_all=count_skp_done=0
        jumlah_total_perhitungan_skp=nilai_total_skp=0
        for target_obj in project_pool.read(cr, uid, target_ids, ['id','nilai_akhir','jumlah_perhitungan','state'
                                                                           ,'work_state'
                                                                           ], context=context) :
            if target_obj['work_state'] == 'done':
                count_skp_done+=1
                nilai_total_skp+=target_obj['nilai_akhir']
                jumlah_total_perhitungan_skp+=target_obj['jumlah_perhitungan']

            count_skp_all+=1

        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['jumlah_perhitungan_skp']=0
        data['nilai_skp']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan']=0
        data['nilai_skp_tambahan_percent']=0
        #data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)
        if count_skp_all > 0:
            data['jml_skp']=count_skp_all
            data['jml_skp_done']=count_skp_done
            data['jml_realisasi_skp']=count_skp_done
            if count_skp_done > 0:
                #Tugas Tambahan Dan Kreatifitas -------
                nilai_tambahan_kreatifitas=fn_nilai_tambahan=fn_nilai_kreatifitas=0
                tambahan_ids = tambahan_pool.search(cr, uid, [('user_id', '=', user_id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', '=' , 'done'),
                                                   ], context=None)


                for tambahan_obj in tambahan_pool.read(cr, uid, tambahan_ids, ['id','nilai_tambahan','nilai_kreatifitas',
                                                                       'nilai_akhir','jumlah_perhitungan','state'
                                                                           ], context=context) :
                    fn_nilai_tambahan = tambahan_obj['nilai_tambahan']
                    fn_nilai_kreatifitas = tambahan_obj['nilai_kreatifitas']
                    nilai_tambahan_kreatifitas=tambahan_obj['nilai_akhir']

                data['fn_nilai_tambahan']=fn_nilai_tambahan
                data['fn_nilai_kreatifitas']=fn_nilai_kreatifitas
                data['jumlah_perhitungan_skp']=nilai_total_skp
                data['nilai_skp']=nilai_total_skp/count_skp_all
                data['nilai_skp_percent']=data['nilai_skp']*0.6
                data['nilai_skp_tambahan']=data['nilai_skp']+nilai_tambahan_kreatifitas
                data['nilai_skp_tambahan_percent']=data['nilai_skp_tambahan']*0.6
                data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)


        data ['nilai_total'] = data['nilai_skp_tambahan_percent'] + data['nilai_perilaku_percent']
        data ['indeks_nilai_total'] = self.get_indeks_nilai(cr,uid,data ['nilai_total'],context=None)
        return data

    def _get_akumulasi_realiasai_per_bulan(self, cr, user_id, employee_id,target_period_year, context=None):


        if not employee_id and not target_period_year :
            return False

        skp_employee_pool = self.pool.get('skp.employee')
        project_pool = self.pool.get('project.project')
        partner_pool = self.pool.get('res.partner')
        tambahan_pool = self.pool.get('project.tambahan.yearly')
        uid = SUPERUSER_ID
        data = {}
        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_perilaku_percent']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan_percent']=0
        data['nilai_skp']=0
        data['nilai_skp_tambahan']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['nilai_total']=0
        data['indeks_nilai_total']=''
        data['indeks_nilai_skp']=''
        data['jumlah_perhitungan_skp']=0
        skp_employee_ids = skp_employee_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        jml_skp=jml_realisasi_skp=count_tambahan_kreatifitas=count=0
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        nilai_total=0
        for skp_obj in  skp_employee_pool.read(cr, uid, skp_employee_ids, ['id','jml_perilaku','nilai_perilaku','nilai_perilaku_percent'
                                                                           ,'nilai_pelayanan','nilai_integritas','nilai_komitmen'
                                                                           ,'nilai_disiplin','nilai_kerjasama','nilai_kepemimpinan'
                                                                           ,'nilai_total'
                                                                           ,'employee_id'
                                                                           ], context=context) :

            #Hitung Perilaku
            jml_perilaku+=skp_obj['jml_perilaku']
            nilai_perilaku+=skp_obj['nilai_perilaku']
            nilai_perilaku_percent+=skp_obj['nilai_perilaku_percent']
            nilai_pelayanan+=skp_obj['nilai_pelayanan']
            nilai_integritas+=skp_obj['nilai_integritas']
            nilai_komitmen+=skp_obj['nilai_komitmen']
            nilai_disiplin+=skp_obj['nilai_disiplin']
            nilai_kerjasama+=skp_obj['nilai_kerjasama']
            nilai_kepemimpinan+=skp_obj['nilai_kepemimpinan']
            count+=1


        if target_period_year:
            data['jml_perilaku']=jml_perilaku #jumlah perilaku yang sd
            data['nilai_perilaku']=nilai_perilaku
            data['nilai_perilaku_percent']=nilai_perilaku_percent
            data['nilai_pelayanan']=nilai_pelayanan
            data['nilai_integritas']=nilai_integritas
            data['nilai_komitmen']=nilai_komitmen
            data['nilai_disiplin']=nilai_disiplin
            data['nilai_kerjasama']=nilai_kerjasama
            data['nilai_kepemimpinan']=nilai_kepemimpinan

            data['count_of_month']=count

            jml_perilaku=12
            if jml_perilaku >0 and count >0  :
                data['nilai_perilaku']=nilai_perilaku/jml_perilaku
                data['nilai_perilaku_percent']=nilai_perilaku_percent/jml_perilaku
                data['nilai_pelayanan']=nilai_pelayanan/jml_perilaku
                data['nilai_integritas']=nilai_integritas/jml_perilaku
                data['nilai_komitmen']=nilai_komitmen/jml_perilaku
                data['nilai_disiplin']=nilai_disiplin/jml_perilaku
                data['nilai_kerjasama']=nilai_kerjasama/jml_perilaku

                data['nilai_kepemimpinan'] =0.0
                data['indeks_nilai_kepemimpinan']=''
                if skp_obj['employee_id']:
                    job_type=''
                    for employee_id_obj in partner_pool.read(cr, uid, [skp_obj['employee_id'][0]], ['id','job_type',], context=context) :
                        job_type = employee_id_obj['job_type']

                    if job_type == 'struktural':
                        data['nilai_kepemimpinan']=nilai_kepemimpinan/jml_perilaku
                        data['indeks_nilai_kepemimpinan']=self.get_indeks_nilai(cr, uid, data['nilai_kepemimpinan'], context)



                data['indeks_nilai_pelayanan']=self.get_indeks_nilai(cr, uid, data['nilai_pelayanan'], context)
                data['indeks_nilai_integritas']=self.get_indeks_nilai(cr, uid, data['nilai_integritas'], context)
                data['indeks_nilai_komitmen']=self.get_indeks_nilai(cr, uid, data['nilai_komitmen'], context)
                data['indeks_nilai_disiplin']=self.get_indeks_nilai(cr, uid, data['nilai_disiplin'], context)
                data['indeks_nilai_kerjasama']=self.get_indeks_nilai(cr, uid, data['nilai_kerjasama'], context)


        #SKP=-----------------
        target_ids = project_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', 'in' , ('confirm','closed'))
                                                   ], context=None)
        count_skp_all=count_skp_done=0
        jumlah_total_perhitungan_skp=nilai_total_skp=0
        for target_obj in project_pool.read(cr, uid, target_ids, ['id','nilai_akhir','jumlah_perhitungan','state'
                                                                           ,'work_state'
                                                                           ], context=context) :
            if target_obj['work_state'] == 'done':
                count_skp_done+=1
                nilai_total_skp+=target_obj['nilai_akhir']
                jumlah_total_perhitungan_skp+=target_obj['jumlah_perhitungan']

            count_skp_all+=1

        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['jumlah_perhitungan_skp']=0
        data['nilai_skp']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan']=0
        data['nilai_skp_tambahan_percent']=0
        #data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)
        if count_skp_all > 0:
            data['jml_skp']=count_skp_all
            data['jml_skp_done']=count_skp_done
            data['jml_realisasi_skp']=count_skp_done
            if count_skp_done > 0:
                #Tugas Tambahan Dan Kreatifitas -------
                nilai_tambahan_kreatifitas=fn_nilai_tambahan=fn_nilai_kreatifitas=0
                tambahan_ids = tambahan_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', '=' , 'done'),
                                                   ], context=None)


                for tambahan_obj in tambahan_pool.read(cr, uid, tambahan_ids, ['id','nilai_tambahan','nilai_kreatifitas',
                                                                       'nilai_akhir','jumlah_perhitungan','state'
                                                                           ], context=context) :
                    fn_nilai_tambahan = tambahan_obj['nilai_tambahan']
                    fn_nilai_kreatifitas = tambahan_obj['nilai_kreatifitas']
                    nilai_tambahan_kreatifitas=tambahan_obj['nilai_akhir']

                data['fn_nilai_tambahan']=fn_nilai_tambahan
                data['fn_nilai_kreatifitas']=fn_nilai_kreatifitas
                data['jumlah_perhitungan_skp']=nilai_total_skp
                data['nilai_skp']=nilai_total_skp/count_skp_all
                data['nilai_skp_percent']=data['nilai_skp']*0.6
                data['nilai_skp_tambahan']=data['nilai_skp']+nilai_tambahan_kreatifitas
                data['nilai_skp_tambahan_percent']=data['nilai_skp_tambahan']*0.6
                data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)


        data ['nilai_total'] = data['nilai_skp_tambahan_percent'] + data['nilai_perilaku_percent']
        data ['indeks_nilai_total'] = self.get_indeks_nilai(cr,uid,data ['nilai_total'],context=None)
        return data

    def _get_akumulasi_realisasi_skp_tambahan_per_bulan(self, cr, user_id, employee_id,target_period_year, context=None):


        if not employee_id and not target_period_year :
            return False

        skp_employee_pool = self.pool.get('skp.employee')
        project_pool = self.pool.get('project.project')
        partner_pool = self.pool.get('res.partner')
        tambahan_pool = self.pool.get('project.tambahan.yearly')
        uid = SUPERUSER_ID
        data = {}
        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_perilaku_percent']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan_percent']=0
        data['nilai_skp']=0
        data['nilai_skp_tambahan']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['nilai_total']=0
        data['indeks_nilai_total']=''
        data['indeks_nilai_skp']=''
        data['jumlah_perhitungan_skp']=0
        skp_employee_ids = skp_employee_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        jml_skp=jml_realisasi_skp=count_tambahan_kreatifitas=count=0
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        nilai_total=0



        #SKP=-----------------
        target_ids = project_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', 'in' , ('confirm','closed'))
                                                   ], context=None)
        count_skp_all=count_skp_done=0
        jumlah_total_perhitungan_skp=nilai_total_skp=0
        for target_obj in project_pool.read(cr, uid, target_ids, ['id','nilai_akhir','jumlah_perhitungan','state'
                                                                           ,'work_state'
                                                                           ], context=context) :
            if target_obj['work_state'] == 'done':
                count_skp_done+=1
                nilai_total_skp+=target_obj['nilai_akhir']
                jumlah_total_perhitungan_skp+=target_obj['jumlah_perhitungan']

            count_skp_all+=1

        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['jumlah_perhitungan_skp']=0
        data['nilai_skp']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan']=0
        data['nilai_skp_tambahan_percent']=0
        #data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)
        if count_skp_all > 0:
            data['jml_skp']=count_skp_all
            data['jml_skp_done']=count_skp_done
            data['jml_realisasi_skp']=count_skp_done
            if count_skp_done > 0:
                #Tugas Tambahan Dan Kreatifitas -------
                nilai_tambahan_kreatifitas=fn_nilai_tambahan=fn_nilai_kreatifitas=0
                tambahan_ids = tambahan_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ('state', '=' , 'done'),
                                                   ], context=None)


                for tambahan_obj in tambahan_pool.read(cr, uid, tambahan_ids, ['id','nilai_tambahan','nilai_kreatifitas',
                                                                       'nilai_akhir','jumlah_perhitungan','state'
                                                                           ], context=context) :
                    fn_nilai_tambahan = tambahan_obj['nilai_tambahan']
                    fn_nilai_kreatifitas = tambahan_obj['nilai_kreatifitas']
                    nilai_tambahan_kreatifitas=tambahan_obj['nilai_akhir']

                data['fn_nilai_tambahan']=fn_nilai_tambahan
                data['fn_nilai_kreatifitas']=fn_nilai_kreatifitas
                data['jumlah_perhitungan_skp']=round(nilai_total_skp,2)
                data['nilai_skp']=round(nilai_total_skp/count_skp_all,2)
                data['nilai_skp_percent']=round(data['nilai_skp']*0.6,2)
                data['nilai_skp_tambahan']=data['nilai_skp']+nilai_tambahan_kreatifitas
                data['nilai_skp_tambahan_percent']=round(data['nilai_skp_tambahan']*0.6,2)
                data['indeks_nilai_skp'] = self.get_indeks_nilai(cr,uid,data ['nilai_skp_tambahan'],context=None)


        data ['nilai_total'] = data['nilai_skp_tambahan_percent']
        data ['indeks_nilai_total'] = '-'#self.get_indeks_nilai(cr,uid,data ['nilai_total'],context=None)
        return data

    def _get_akumulasi_realisasi_perilaku_per_bulan(self, cr, user_id, employee_id,target_period_year, context=None):


        if not employee_id and not target_period_year :
            return False

        skp_employee_pool = self.pool.get('skp.employee')
        project_pool = self.pool.get('project.project')
        partner_pool = self.pool.get('res.partner')
        tambahan_pool = self.pool.get('project.tambahan.yearly')
        uid = SUPERUSER_ID
        data = {}
        data['jml_skp']=0
        data['jml_skp_done']=0
        data['jml_realisasi_skp']=0
        data['indeks_nilai_pelayanan']=''
        data['indeks_nilai_integritas']=''
        data['indeks_nilai_komitmen']=''
        data['indeks_nilai_disiplin']=''
        data['indeks_nilai_kerjasama']=''
        data['indeks_nilai_kepemimpinan']=''
        data['nilai_perilaku_percent']=0
        data['nilai_skp_percent']=0
        data['nilai_skp_tambahan_percent']=0
        data['nilai_skp']=0
        data['nilai_skp_tambahan']=0
        data['fn_nilai_tambahan']=0
        data['fn_nilai_kreatifitas']=0
        data['nilai_total']=0
        data['indeks_nilai_total']=''
        data['indeks_nilai_skp']=''
        data['jumlah_perhitungan_skp']=0
        skp_employee_ids = skp_employee_pool.search(cr, uid, [('user_id', '=', user_id.id),
                                                   ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        jml_skp=jml_realisasi_skp=count_tambahan_kreatifitas=count=0
        nilai_skp_tambahan_percent=nilai_skp=jml_skp=nilai_skp_percent=jml_perilaku=nilai_perilaku=nilai_perilaku_percent=0
        nilai_pelayanan=nilai_integritas=nilai_komitmen=nilai_disiplin=nilai_kerjasama=nilai_kepemimpinan=0
        nilai_total=0
        for skp_obj in  skp_employee_pool.read(cr, uid, skp_employee_ids, ['id','jml_perilaku','nilai_perilaku','nilai_perilaku_percent'
                                                                           ,'nilai_pelayanan','nilai_integritas','nilai_komitmen'
                                                                           ,'nilai_disiplin','nilai_kerjasama','nilai_kepemimpinan'
                                                                           ,'nilai_total'
                                                                           ,'employee_id'
                                                                           ], context=context) :

            #Hitung Perilaku
            jml_perilaku+=skp_obj['jml_perilaku']
            nilai_perilaku+=skp_obj['nilai_perilaku']
            nilai_perilaku_percent+=skp_obj['nilai_perilaku_percent']
            nilai_pelayanan+=skp_obj['nilai_pelayanan']
            nilai_integritas+=skp_obj['nilai_integritas']
            nilai_komitmen+=skp_obj['nilai_komitmen']
            nilai_disiplin+=skp_obj['nilai_disiplin']
            nilai_kerjasama+=skp_obj['nilai_kerjasama']
            nilai_kepemimpinan+=skp_obj['nilai_kepemimpinan']
            count+=1


        if target_period_year:
            data['jml_perilaku']=jml_perilaku #jumlah perilaku yang sd
            data['nilai_perilaku']=round(nilai_perilaku,2)
            data['nilai_perilaku_percent']=round(nilai_perilaku_percent,2)
            data['nilai_pelayanan']=nilai_pelayanan
            data['nilai_integritas']=nilai_integritas
            data['nilai_komitmen']=nilai_komitmen
            data['nilai_disiplin']=nilai_disiplin
            data['nilai_kerjasama']=nilai_kerjasama
            data['nilai_kepemimpinan']=nilai_kepemimpinan

            data['count_of_month']=count

            jml_perilaku=12
            if jml_perilaku >0 and count >0  :
                data['nilai_perilaku']=round(nilai_perilaku/jml_perilaku,2)
                data['nilai_perilaku_percent']=round(nilai_perilaku_percent/jml_perilaku,2)
                data['nilai_pelayanan']=round(nilai_pelayanan/jml_perilaku,2)
                data['nilai_integritas']=round(nilai_integritas/jml_perilaku,2)
                data['nilai_komitmen']=round(nilai_komitmen/jml_perilaku,2)
                data['nilai_disiplin']=round(nilai_disiplin/jml_perilaku,2)
                data['nilai_kerjasama']=round(nilai_kerjasama/jml_perilaku,2)

                data['nilai_kepemimpinan'] =0.0
                data['indeks_nilai_kepemimpinan']=''
                if skp_obj['employee_id']:
                    job_type=''
                    for employee_id_obj in partner_pool.read(cr, uid, [skp_obj['employee_id'][0]], ['id','job_type',], context=context) :
                        job_type = employee_id_obj['job_type']

                    if job_type == 'struktural':
                        data['nilai_kepemimpinan']=round(nilai_kepemimpinan/jml_perilaku,2)
                        data['indeks_nilai_kepemimpinan']=self.get_indeks_nilai(cr, uid, data['nilai_kepemimpinan'], context)



                data['indeks_nilai_pelayanan']=self.get_indeks_nilai(cr, uid, data['nilai_pelayanan'], context)
                data['indeks_nilai_integritas']=self.get_indeks_nilai(cr, uid, data['nilai_integritas'], context)
                data['indeks_nilai_komitmen']=self.get_indeks_nilai(cr, uid, data['nilai_komitmen'], context)
                data['indeks_nilai_disiplin']=self.get_indeks_nilai(cr, uid, data['nilai_disiplin'], context)
                data['indeks_nilai_kerjasama']=self.get_indeks_nilai(cr, uid, data['nilai_kerjasama'], context)


        #data ['nilai_total'] = data['nilai_skp_tambahan_percent'] + data['nilai_perilaku_percent']
        #data ['indeks_nilai_total'] = self.get_indeks_nilai(cr,uid,data ['nilai_total'],context=None)
        return data
skp_employee_yearly()




