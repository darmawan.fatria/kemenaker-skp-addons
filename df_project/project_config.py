# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
# ====================== config ================================= #

class satuan_hitung(osv.Model):
    _name = "satuan.hitung"
    _description    ="Satuan Hitung SKP"
   
    _columns = {
        'name'             : fields.char('Satuan',size=50,required=True),  
        'description'      : fields.text('Deskripsi'),
        'active'           : fields.boolean('Aktif'),
    }
    _defaults ={
                'active' : 'True',
                }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
satuan_hitung()


class config_skp(osv.Model):
    _name = "config.skp"
    _description    ="Konfigurasi SKP"
   
    _columns = {
        'name'     : fields.text('Nama',required=True),
        'code'     : fields.char('Kode',size=8,required=True),    
        'type'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('nilai', 'Penilaian'),
                                       ],'Tipe',required=True),
        'config_value_int'             : fields.integer('Nilai Integer'),
        'config_value_float'           : fields.float('Nilai Float'),
        'config_value_string'          : fields.char('Nilai String',size=256),
        'active'     : fields.boolean('Aktif'),
    }
    _sql_constraints = [
         ('name_uniq', 'unique (name,code,type)',
             'Data Tidak Bisa Dimasukan, Satuan Hitung Dengan Nama Ini Sudah Tersedia')
     ]
config_skp()
class config_poin_index(osv.Model):
    _name = "config.poin.index"
    _description    ="Konversi Nilai"
   
    _columns = {
       
        'name'     : fields.char('Nama',size=25,required=True),    
        'type'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('nilai', 'Penilaian'),
                                       ],'Tipe',required=True),
        'value_from'             : fields.integer('Batas Nilai Bawah',required=True),
        'value_to'             : fields.float('Batas Nilai Atas',required=True),
        'value_result'          : fields.char('Hasil',size=256),
        'image_path'          : fields.char('Gambar',size=256),
        'description'     : fields.text('Deskripsi'),
        'active'     : fields.boolean('Aktif'),
    }
    _sql_constraints = [
         ('name_uniq', 'unique (name,type)',
             'Data Tidak Bisa Dimasukan, Satuan Hitung Dengan Nama Ini Sudah Tersedia')
     ]
config_poin_index()
class lookup_kegiatan(osv.Model):
    _name = "lookup.kegiatan"
    _description    ="Kegiatan"
   
    _columns = {
        'code'     : fields.char('Kode Kegiatan',size=30,required=False),
        'name'     : fields.char('Nama Kegiatan',size=400,required=True),    
        'target_category': fields.selection([('bulanan', 'Bulanan'), ('tahunan', 'Tahunan')], 'Kategori',required=True,),
        'target_type_id': fields.selection([         ('dpa_opd_biro', 'Kegiatan APBD - DPA'),   
                                                      ('dipa_apbn', 'Kegiatan APBN - DIPA'),  
                                                      ('sotk', 'Kegiatan Tupoksi (Non DPA)'), 
                                                      ('lain_lain', 'Kegiatan Target Pendapatan Daerah'), 
                                                      ],
                                                      'Jenis Kegiatan', 
                                                     ),
        'control_biaya'             : fields.float('Verifikasi Biaya'),
        'period_month'              : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')],'Periode Bulan' ,required=True, 
                                                     ),
        'period_year'     : fields.char('Periode Tahun',size=4, required=True, ),
        'description'     : fields.text('Deskripsi'),
        'company_id': fields.many2one('res.company', 'Instansi',),
        'induk_kegiatan_id': fields.many2one('lookup.kegiatan', 'Induk Kegiatan',),
        'active'     : fields.boolean('Aktif'),
    }
lookup_kegiatan()


class verifikasi_absen_pegawai(osv.Model):
    _name = "verifikasi.absen.pegawai"
    _description    ="Lookup Absensi Pegawai"
   
    _columns = {
        'employee_id': fields.many2one('res.partner', 'Pegawai'),
        'nip': fields.related('employee_id', 'nip', type='char', string="NIP",size=20, readonly=True),
        'target_period_year'     : fields.char('Periode Tahun',size=4, required=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')],'Periode Bulan'
                                                     , required=True),
        'jumlah_apel_pagi'     : fields.integer('Jumlah Apel Pagi'),
        'jumlah_upacara_hari_besar': fields.integer( 'Jumlah Upacara Hari Besar'), 
        'jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja (Hari)' ),   
        'jumlah_jam_kerja': fields.integer( 'Jumlah Jam Kerja (Jam)'),
        'notes'     : fields.text('Catatan'),
                
    }
    #_sql_constraints = [
    #     ('name_uniq', 'unique (employee_id,target_period_year,target_period_month)',
    #         'Data verifikasi pegawai untuk  bulan dan tahun yang sama tidak boleh duplikat')
    # ]
verifikasi_absen_pegawai()

class hari_kerja_bulanan(osv.Model):
    _name = "hari.kerja.bulanan"
    _description    ="Lookup Komitmen"
   
    _columns = {
        'target_period_year'     : fields.char('Periode Tahun',size=4, required=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')],'Periode Bulan'
                                                     , required=True),
        'jumlah_apel_pagi'     : fields.integer('Jumlah Apel Pagi'),
        'jumlah_upacara_hari_besar': fields.integer( 'Jumlah Upacara Hari Besar'), 
        'jumlah_hari_kerja'     : fields.integer('Jumlah Hari Kerja (Hari)' ),   
        'jumlah_jam_kerja': fields.integer( 'Jumlah Jam Kerja (Jam)'),  
        'notes'     : fields.text('Catatan'),
    }
    _sql_constraints = [
         ('name_uniq', 'unique (target_period_year,target_period_month)',
             'Data periode bulan dan tahun tidak boleh duplikat')
     ]
hari_kerja_bulanan()


class acuan_penilaian(osv.Model):
    _name = "acuan.penilaian"
    _description    ="Acuan Penilaian"
   
    def get_selisih_batas_atas_bawah(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for data in self.browse(cr, uid, ids, context=context):
            res[data.id] = 0
            if data.nilai_atas and data.nilai_bawah :
                res[data.id] = data.nilai_atas - data.nilai_bawah
            
        return res 
    
    _columns = {
         'name'     : fields.char('Satuan',size=62,required=True),
        'code'     : fields.char('kode',size=32,required=True),    
        'kategori_nilai'     : fields.selection([('fix', 'Fix'),('threshold', 'Threshold')],"Kategori Penilaian"),
        'type'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('orientasi', 'Penilaian Perilaku Dari Sisi Orientasi'),
                                       ('integritas', 'Penilaian Perilaku Dari Sisi Integritas'),
                                       ('kerjasama', 'Penilaian Perilaku Dari Sisi Kerjasama'),
                                       ('kepemimpinan', 'Penilaian Perilaku Dari Sisi Kepemimpinan'),
                                       ('tugas_tambahan', 'Penilaian Perilaku Dari Sisi Tugas Tambahan'),
                                       ('kreatifitas', 'Penilaian Perilaku Dari Sisi Kreatifitas'),
                                       ],'Tipe',required=True),
        'kategori_integritas'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('presiden', 'Presiden'),
                                       ('gubernur', 'Gubernur'),
                                       ('kepalaopd', 'Kepala Daerah'),
                                       ('atasan', 'Atasan Langsung'),
                                       ('pejabat_es1', 'Pejabat Eselon I'),
                                       ('pejabat_es2', 'Pejabat Eselon II'),
                                       ('pejabat_es3', 'Pejabat Eselon III'),
                                       ('pejabat_es4', 'Pejabat Eselon IV'),
                                        ('tidak_hukuman', 'Tidak Ada Hukuman'),
                                       ('hukuman_berat', 'Hukuman Berat'),
                                       ('hukuman_sedang', 'Hukuman Sedang'),
                                       ('hukuman_ringan', 'Hukuman Ringan'),
                                       ],'Kategori Penilaian Integritas'),
        'kategori_kerjasama'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('nasional', 'Panitia/tim/pokja Nasional'),
                                       ('provinsi', 'Panitia/tim/pokja Provinsi'),
                                       ('perangkat_daerah', 'Panitia/tim/pokja Perangkat Daerah'),
                                       ('atasan', 'Panitia/tim/pokja Unit Kerja Atasan Langsung'),
                                       ('rapat_atasan', 'Rapat kerja/brefing Unit Kerja Atasan langsung'),
                                       ('rapat_perangkat_daerah', 'Rapat kerja/brefing Perangkat Daerah'),
                                       ('rapat_provinsi', 'Rapat kerja/brefing Provinsi'),
                                       ('rapat_nasional', 'Rapat kerja/brefing Nasional'),
                                       ],'Kategori Penilaian Kerjasama'),
        'kategori_kepemimpinan'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('nasional', 'Panitia/tim/pokja Nasional'),
                                       ('provinsi', 'Panitia/tim/pokja Provinsi'),
                                       ('perangkat_daerah', 'Panitia/tim/pokja Perangkat Daerah'),
                                       ('unitkerja', 'Panitia/tim/pokja Unit Kerja'),
                                       ('narsum_unitkerja', 'Narasumber Kerja Atasan langsung'),
                                       ('narsum_perangkat_daerah', 'Narasumber Perangkat Daerah'),
                                       ('narsum_provinsi', 'Narasumber Provinsi'),
                                       ('narsum_nasional', 'Narasumber Nasional'),
                                       ],'Kategori Penilaian Kepemimpinan'),
        'kategori_kreatifitas'     : fields.selection([('lain_lain', 'Lain-Lain'),
                                       ('presiden', 'Presiden'),
                                       ('gubernur', 'Gubernur'),
                                       ('kepalaopd', 'Kepala Daerah')],'Kategori Penilaian Kreatifitas'),
        'kategori_orientasi'     : fields.selection([('ketepatan_laporan_spj', 'Ketepatan Laporan SPJ'),
                                       ('ketepatan_laporan_ukp4', 'Ketepatan Laporan UKP4'),
                                       ('efisiensi_biaya_operasional', 'Efisiensi Biaya Operasional'),
                                       ],'Kategori Penilaian Orientasi'),
        'nilai_tunggal'     : fields.integer('Nilai Tunggal'),
        'nilai_tambahan'     : fields.integer('Nilai Tambahan'),
        'nilai_atas'     : fields.integer('Nilai Batas Atas'),
        'nilai_bawah'     : fields.integer('Nilai Batas Bawah'),
        'active'     : fields.boolean('Aktif'),
    }
    _sql_constraints = [
         ('name_uniq', 'unique (code,type,category)',
             'Data Tidak Bisa Dimasukan, Satuan Hitung Dengan Nama Ini Sudah Tersedia')
     ]
acuan_penilaian()

