from openerp.osv import fields, osv
from datetime import datetime, date
import time
from mx import DateTime

class notification_skp_employee_process(osv.osv_memory):
    _name = "notification.skp.employee.process"
    _columns = {
        'name': fields.char('Notif', size=128),
    }
notification_skp_employee_process();
class skp_employee_settings(osv.osv_memory):
    _name = 'skp.employee.settings'
    _description = 'Konfigurasi Aplikasi SKP'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP SIPKD', size=64, readonly=True),
            'param_type'     : fields.selection([('all', 'Semua'), ('skp', 'SKP'),
                                                      ('perilaku', 'Perilaku'), ('tk', 'Tambahan Dan Kreatifitas')], 'Jenis Kegiatan'
                                                     ),
            'param_list_of_ids' : fields.char('Long Char', size=400, ),
             'param_employee_id': fields.many2one('res.partner', 'Pegawai',),

            'param_target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'param_all_company'     : fields.boolean('Semua DITJEN',),
            'param_target_period_year'     : fields.char('Periode Tahun', size=4, ),
        'param_custom_integer'     : fields.integer('Param Custom', size=2, ),
            'param_company_id' : fields.many2one('res.company','DITJEN')
    }
    _defaults = {
        'param_custom_integer' :-1,
    }

    def auto_recalculate_by_month_scheduler(self, cr, uid,param_year,param_company_id,delta_days, context=None):
        print "param_year  ",param_year
        print "param_company_id  ",param_company_id
        result =  self.action_recalculate_monthly_param(cr,uid,param_year,param_company_id,delta_days);
        print "monthly calculation done...."

        return result
    def auto_recalculate_by_year_scheduler(self, cr, uid,param_year,param_company_id, context=None):
        print "param_year  ",param_year
        print "param_company_id  ",param_company_id
        result =  self.action_recalculate_yearly_param(cr,uid,param_year,param_company_id);
        print "yearly calculation done...."

        return result

    def action_recalculate_monthly_param(self, cr, uid, param_year,param_company_id,delta_days, context=None):

        #
        #
        #for param_obj in self.browse(cr, uid, ids, context=context):
                    partner_pool = self.pool.get('res.partner')
                    skp_employee_pool = self.pool.get('skp.employee')
                #try :

                    param_employee_id=False
                    is_single_calc=False


                    if not param_employee_id :
                        #print "all emp"
                        if param_company_id:
                            employee_ids=partner_pool.search(cr, uid, [('company_id','in',param_company_id),('employee','=',True)
                                                         ], context=None)
                        else :
                            employee_ids=partner_pool.search(cr, uid, [('employee','=',True)
                                                         ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)



                    for employee_obj in employee_objs:

                        #try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id

                            now=DateTime.today();


                            curr_date =  DateTime.strptime(now.strftime('%Y-%m-%d'),'%Y-%m-%d')
                            minus_1_date = curr_date + DateTime.RelativeDateTime(days=delta_days)
                            now_day=curr_date.strftime('%Y-%m-%d')
                            day_minus_1=minus_1_date.strftime('%Y-%m-%d')
                            domain = param_year,employee_user_id,day_minus_1,now_day,param_year,employee_user_id,day_minus_1,now_day,param_year,employee_user_id,day_minus_1,now_day

                            list_period_month = self.get_period_months(cr,uid,domain)

                            if list_period_month:
                                for p_month in list_period_month :
                                    single_month = p_month and p_month[0] or '00'
                                    skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                       ('target_period_year', '=', param_year),
                                                                    ('target_period_month', '=', single_month),

                                                                       ], context=None)

                                    for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                        #try :

                                                skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent,fn_nilai_tambahan,fn_nilai_kreatifitas,nilai_skp_tambahan_percent,jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan,nilai_total,nilai_tpp =skp_employee_pool._get_detail_nilai_skp_employee(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)


                                                update_values = {
                                                    'skp_state_count': skp_state_count,
                                                    'jml_skp': jml_skp,
                                                    'jml_all_skp': jml_all_skp,
                                                    'nilai_skp': nilai_skp,
                                                    'fn_nilai_tambahan': fn_nilai_tambahan,
                                                    'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                                    'nilai_skp_percent': nilai_skp_percent,
                                                    'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                                    'jml_perilaku': jml_perilaku,
                                                    'nilai_perilaku': nilai_perilaku,
                                                    'nilai_perilaku_percent': nilai_perilaku_percent,
                                                    'nilai_pelayanan': nilai_pelayanan,
                                                     'nilai_integritas': nilai_integritas,
                                                     'nilai_komitmen':nilai_komitmen,
                                                     'nilai_disiplin': nilai_disiplin,
                                                     'nilai_kerjasama': nilai_kerjasama,
                                                     'nilai_kepemimpinan':nilai_kepemimpinan,

                                                    'nilai_total': nilai_total,
                                                    'nilai_tpp': nilai_tpp,
                                                }
                                                skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                #except:
                                    #print "Some process cannot be calculate on calculate monthly";


                        #except:
                            #print "Some process cannot be calculate on load person";
                #except:
                #    print "Some process cannot be calculate on loop param";


                    return True

    def action_recalculate_yearly_param(self, cr, uid, param_year,param_company_id,context={}):


        #for param_obj in self.browse(cr, uid, ids, context=context):
                partner_pool = self.pool.get('res.partner')
                skp_employee_pool = self.pool.get('skp.employee')
                try :
                    #param_year = param_obj.param_target_period_year
                    #param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False


                    if param_year :
                        #print "all emp"
                        if param_company_id :
                            employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                         ], context=None)
                        else :
                            employee_ids=partner_pool.search(cr, uid, [('employee','=',True)
                                                         ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)



                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id




                            if is_can_calculate :
                                self.do_all_summary_calculation(cr,uid,employee_user_id,employee_id,param_year)
                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


                return True;

    def action_recalculate_by_company_monthly_param_all(self, cr, uid, ids, param_month,context={}):


        for param_obj in self.browse(cr, uid, ids, context=context):
            self.action_recalculate_by_company_monthly_param(cr,uid,param_obj,False)

        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.skp.employee.process',
                'target': 'new',
                'context': context,
            }

    def action_recalculate_by_company_monthly_param(self, cr, uid, param_obj, param_month,context={}):

        #
        #
        #for param_obj in self.browse(cr, uid, ids, context=context):
                partner_pool = self.pool.get('res.partner')
                skp_employee_pool = self.pool.get('skp.employee')
                try :
                    param_year = param_obj.param_target_period_year
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False


                    if not param_obj.param_employee_id :
                        #print "all emp"
                        employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                     ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"


                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id

                            if param_month:
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),
                                                                ('target_period_month', '=', param_month),

                                                                   ], context=None)
                            else :
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),

                                                                   ], context=None)

                            for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                try :

                                        skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent,fn_nilai_tambahan,fn_nilai_kreatifitas,nilai_skp_tambahan_percent,jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan,nilai_total,nilai_tpp =skp_employee_pool._get_detail_nilai_skp_employee(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)


                                        update_values = {
                                            'skp_state_count': skp_state_count,
                                            'jml_skp': jml_skp,
                                            'jml_all_skp': jml_all_skp,
                                            'nilai_skp': nilai_skp,
                                            'fn_nilai_tambahan': fn_nilai_tambahan,
                                            'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                            'nilai_skp_percent': nilai_skp_percent,
                                            'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                            'jml_perilaku': jml_perilaku,
                                            'nilai_perilaku': nilai_perilaku,
                                            'nilai_perilaku_percent': nilai_perilaku_percent,
                                            'nilai_pelayanan': nilai_pelayanan,
                                             'nilai_integritas': nilai_integritas,
                                             'nilai_komitmen':nilai_komitmen,
                                             'nilai_disiplin': nilai_disiplin,
                                             'nilai_kerjasama': nilai_kerjasama,
                                             'nilai_kepemimpinan':nilai_kepemimpinan,

                                            'nilai_total': nilai_total,
                                            'nilai_tpp': nilai_tpp,
                                        }
                                        skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                except:
                                    print "Some process cannot be calculate on calculate monthly";


                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


                return True


    def action_recalculate_by_company_and_year(self, cr, uid, ids, context={}):

        partner_pool = self.pool.get('res.partner')
        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                try :
                    param_year = param_obj.param_target_period_year
                    param_month = param_obj.param_target_period_month
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False



                    if not param_obj.param_employee_id :
                        #print "all emp"
                        employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                     ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"


                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id

                            if param_month:
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),
                                                                ('target_period_month', '=', param_month),

                                                                   ], context=None)
                            else :
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),

                                                                   ], context=None)

                            for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                try :

                                        skp_state_count = skp_employee_obj.skp_state_count;
                                        jml_skp = skp_employee_obj.jml_skp;
                                        jml_all_skp = skp_employee_obj.jml_all_skp;

                                        fn_nilai_tambahan = skp_employee_obj.fn_nilai_tambahan;
                                        fn_nilai_kreatifitas = skp_employee_obj.fn_nilai_kreatifitas;

                                        nilai_skp = skp_employee_obj.nilai_skp;
                                        nilai_skp_percent = skp_employee_obj.nilai_skp_percent;
                                        nilai_skp_tambahan_percent = skp_employee_obj.nilai_skp_tambahan_percent


                                        jml_perilaku = skp_employee_obj.jml_perilaku;
                                        nilai_perilaku = skp_employee_obj.nilai_perilaku;
                                        nilai_perilaku_percent = skp_employee_obj.nilai_perilaku_percent;
                                        nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan = skp_employee_pool.get_detail_nilai_perilaku(cr,uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,context)


                                        nilai_total = skp_employee_obj.nilai_total;
                                        nilai_tpp = skp_employee_obj.nilai_tpp;

                                        update_values = {
                                            'skp_state_count': skp_state_count,
                                            'jml_skp': jml_skp,
                                            'jml_all_skp': jml_all_skp,
                                            'nilai_skp': nilai_skp,
                                            'fn_nilai_tambahan': fn_nilai_tambahan,
                                            'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                            'nilai_skp_percent': nilai_skp_percent,
                                            'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                            'jml_perilaku': jml_perilaku,
                                            'nilai_perilaku': nilai_perilaku,
                                            'nilai_perilaku_percent': nilai_perilaku_percent,
                                            'nilai_pelayanan': nilai_pelayanan,
                                             'nilai_integritas': nilai_integritas,
                                             'nilai_komitmen':nilai_komitmen,
                                             'nilai_disiplin': nilai_disiplin,
                                             'nilai_kerjasama': nilai_kerjasama,
                                             'nilai_kepemimpinan':nilai_kepemimpinan,

                                            'nilai_total': nilai_total,
                                            'nilai_tpp': nilai_tpp,
                                        }
                                        skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                except:
                                    print "Some process cannot be calculate on calculate monthly";

                            if is_can_calculate :
                                self.do_all_summary_calculation(cr,uid,employee_user_id,employee_id,param_year)
                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";

        
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.skp.employee.process',
                'target': 'new',
                'context': context,
            }

    def action_recalculate_by_company_monthly_better_optimation(self, cr, uid, ids, context={}):

        partner_pool = self.pool.get('res.partner')
        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                try :
                    param_year = param_obj.param_target_period_year
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False


                    if not param_obj.param_employee_id :
                        if not param_obj.param_all_company :
                            employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                         ], context=None)
                        else :
                            employee_ids=partner_pool.search(cr, uid, [('employee','=',True)
                                                         ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"


                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id



                            now=DateTime.today();
                            delta_days = -1
                            if param_obj.param_custom_integer :
                                delta_days = param_obj.param_custom_integer

                            curr_date =  DateTime.strptime(now.strftime('%Y-%m-%d'),'%Y-%m-%d')
                            minus_1_date = curr_date + DateTime.RelativeDateTime(days=delta_days)
                            now_day=curr_date.strftime('%Y-%m-%d')
                            day_minus_1=minus_1_date.strftime('%Y-%m-%d')
                            domain = param_year,employee_user_id,day_minus_1,now_day,param_year,employee_user_id,day_minus_1,now_day,param_year,employee_user_id,day_minus_1,now_day

                            list_period_month = self.get_period_months(cr,uid,domain)

                            if list_period_month:
                                for p_month in list_period_month :
                                    single_month = p_month and p_month[0] or '00'
                                    skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                       ('target_period_year', '=', param_year),
                                                                    ('target_period_month', '=', single_month),

                                                                       ], context=None)



                                    for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                        try :

                                                skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent,fn_nilai_tambahan,fn_nilai_kreatifitas,nilai_skp_tambahan_percent,jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan,nilai_total,nilai_tpp =skp_employee_pool._get_detail_nilai_skp_employee(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)


                                                update_values = {
                                                    'skp_state_count': skp_state_count,
                                                    'jml_skp': jml_skp,
                                                    'jml_all_skp': jml_all_skp,
                                                    'nilai_skp': nilai_skp,
                                                    'fn_nilai_tambahan': fn_nilai_tambahan,
                                                    'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                                    'nilai_skp_percent': nilai_skp_percent,
                                                    'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                                    'jml_perilaku': jml_perilaku,
                                                    'nilai_perilaku': nilai_perilaku,
                                                    'nilai_perilaku_percent': nilai_perilaku_percent,
                                                    'nilai_pelayanan': nilai_pelayanan,
                                                     'nilai_integritas': nilai_integritas,
                                                     'nilai_komitmen':nilai_komitmen,
                                                     'nilai_disiplin': nilai_disiplin,
                                                     'nilai_kerjasama': nilai_kerjasama,
                                                     'nilai_kepemimpinan':nilai_kepemimpinan,

                                                    'nilai_total': nilai_total,
                                                    'nilai_tpp': nilai_tpp,
                                                }
                                                skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                        except:
                                            print "Some process cannot be calculate on calculate monthly";


                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.skp.employee.process',
                'target': 'new',
                'context': context,
            }

    def get_period_months(self, cr, uid, domain, context={}):
        query = """
                                 SELECT  distinct target_period_month
                                    from project_perilaku
                                    where
                                     target_period_year = %s
                                      and (state='done' or state='evaluated')
                                     and user_id = %s
                                     and to_date(to_char(write_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') between to_date(%s, 'YYYY-MM-DD') and to_date(%s, 'YYYY-MM-DD')
                                     and active
                                    union
                                    SELECT  distinct target_period_month
                                    from project_tambahan_kreatifitas
                                    where
                                    target_period_year = %s
                                    and user_id = %s
                                    and to_date(to_char(write_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') between to_date(%s, 'YYYY-MM-DD')   and to_date(%s, 'YYYY-MM-DD')
                                    and (state='done' or state='evaluated')
                                    and active
                                    union
                                    SELECT  distinct target_period_month
                                    from project_skp
                                    where
                                    target_period_year = %s
                                    and user_id = %s
                                    and to_date(to_char(write_date, 'YYYY-MM-DD'), 'YYYY-MM-DD') between to_date(%s, 'YYYY-MM-DD')   and to_date(%s, 'YYYY-MM-DD')
                                    and (state='done' or state='closed' or state='evaluated')
                                    and active

                                """
        cr.execute(query,domain)
        rs_query_month = cr.fetchall()
        list_period_month = []
        for t_month in rs_query_month:
            list_period_month.append(t_month)

        return list_period_month;

    def action_recalculate_by_company_monthly_optimation(self, cr, uid, ids, context={}):

        partner_pool = self.pool.get('res.partner')
        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                try :
                    param_year = param_obj.param_target_period_year
                    param_month = param_obj.param_target_period_month
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False


                    if not param_obj.param_employee_id :
                        if not param_obj.param_all_company :
                            employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                         ], context=None)
                        else :
                            employee_ids=partner_pool.search(cr, uid, [('employee','=',True)
                                                         ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"


                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id

                            if param_month:
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),
                                                                ('target_period_month', '=', param_month),

                                                                   ], context=None)
                            else :
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),

                                                                   ], context=None)

                            for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                try :

                                        '''skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent=skp_employee_pool._get_detail_nilai_skp(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)
                                        fn_nilai_tambahan,fn_nilai_kreatifitas=skp_employee_pool._get_detail_nilai_tambahan(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)

                                        if nilai_skp:
                                            nilai_skp_tambahan_percent = ( nilai_skp * 0.6 )  + (fn_nilai_tambahan or 0) + (fn_nilai_kreatifitas or 0)
                                        else :
                                            nilai_skp_tambahan_percent =0.0

                                        jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan = skp_employee_pool._get_detail_nilai_all_perilaku(cr,uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,context)

                                        nilai_total = 0.0
                                        nilai_tpp=0
                                        if nilai_skp_tambahan_percent and nilai_skp_tambahan_percent>0.0:
                                            nilai_total +=  nilai_skp_tambahan_percent
                                        if nilai_perilaku_percent and nilai_perilaku_percent>0.0:
                                            nilai_total +=  nilai_perilaku_percent
                                            '''
                                        skp_state_count,jml_skp,jml_all_skp,nilai_skp,nilai_skp_percent,fn_nilai_tambahan,fn_nilai_kreatifitas,nilai_skp_tambahan_percent,jml_perilaku,nilai_perilaku,nilai_perilaku_percent,nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan,nilai_total,nilai_tpp =skp_employee_pool._get_detail_nilai_skp_employee(cr, uid, skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year, context=None)


                                        update_values = {
                                            'skp_state_count': skp_state_count,
                                            'jml_skp': jml_skp,
                                            'jml_all_skp': jml_all_skp,
                                            'nilai_skp': nilai_skp,
                                            'fn_nilai_tambahan': fn_nilai_tambahan,
                                            'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                            'nilai_skp_percent': nilai_skp_percent,
                                            'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                            'jml_perilaku': jml_perilaku,
                                            'nilai_perilaku': nilai_perilaku,
                                            'nilai_perilaku_percent': nilai_perilaku_percent,
                                            'nilai_pelayanan': nilai_pelayanan,
                                             'nilai_integritas': nilai_integritas,
                                             'nilai_komitmen':nilai_komitmen,
                                             'nilai_disiplin': nilai_disiplin,
                                             'nilai_kerjasama': nilai_kerjasama,
                                             'nilai_kepemimpinan':nilai_kepemimpinan,

                                            'nilai_total': nilai_total,
                                            'nilai_tpp': nilai_tpp,
                                        }
                                        skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                except:
                                    print "Some process cannot be calculate on calculate monthly";

                            #if is_can_calculate :
                            #    self.do_all_summary_calculation(cr,uid,employee_user_id,employee_id,param_year)
                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.skp.employee.process',
                'target': 'new',
                'context': context,
            }


    def action_recalculate_by_company_monthly(self, cr, uid, ids, context={}):

        partner_pool = self.pool.get('res.partner')
        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                try :
                    param_year = param_obj.param_target_period_year
                    param_month = param_obj.param_target_period_month
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False


                    if not param_obj.param_employee_id :
                        #print "all emp"
                        employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                     ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"


                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id

                            if param_month:
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),
                                                                ('target_period_month', '=', param_month),

                                                                   ], context=None)
                            else :
                                skp_employee_ids = skp_employee_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                                   ('target_period_year', '=', param_year),

                                                                   ], context=None)

                            print "employee ids : ",skp_employee_ids
                            print "Param Month ",param_month


                            for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_employee_ids, context=context):
                                try :

                                        skp_state_count = skp_employee_obj.skp_state_count;
                                        jml_skp = skp_employee_obj.jml_skp;
                                        jml_all_skp = skp_employee_obj.jml_all_skp;

                                        fn_nilai_tambahan = skp_employee_obj.fn_nilai_tambahan;
                                        fn_nilai_kreatifitas = skp_employee_obj.fn_nilai_kreatifitas;

                                        nilai_skp = skp_employee_obj.nilai_skp;
                                        nilai_skp_percent = skp_employee_obj.nilai_skp_percent;
                                        nilai_skp_tambahan_percent = skp_employee_obj.nilai_skp_tambahan_percent


                                        jml_perilaku = skp_employee_obj.jml_perilaku;
                                        nilai_perilaku = skp_employee_obj.nilai_perilaku;
                                        nilai_perilaku_percent = skp_employee_obj.nilai_perilaku_percent;
                                        nilai_pelayanan,nilai_disiplin,nilai_komitmen,nilai_integritas,nilai_kerjasama,nilai_kepemimpinan = skp_employee_pool.get_detail_nilai_perilaku(cr,uid,skp_employee_obj.user_id.id,skp_employee_obj.target_period_month,skp_employee_obj.target_period_year,context)


                                        nilai_total = skp_employee_obj.nilai_total;
                                        nilai_tpp = skp_employee_obj.nilai_tpp;

                                        update_values = {
                                            'skp_state_count': skp_state_count,
                                            'jml_skp': jml_skp,
                                            'jml_all_skp': jml_all_skp,
                                            'nilai_skp': nilai_skp,
                                            'fn_nilai_tambahan': fn_nilai_tambahan,
                                            'fn_nilai_kreatifitas': fn_nilai_kreatifitas,
                                            'nilai_skp_percent': nilai_skp_percent,
                                            'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,

                                            'jml_perilaku': jml_perilaku,
                                            'nilai_perilaku': nilai_perilaku,
                                            'nilai_perilaku_percent': nilai_perilaku_percent,
                                            'nilai_pelayanan': nilai_pelayanan,
                                             'nilai_integritas': nilai_integritas,
                                             'nilai_komitmen':nilai_komitmen,
                                             'nilai_disiplin': nilai_disiplin,
                                             'nilai_kerjasama': nilai_kerjasama,
                                             'nilai_kepemimpinan':nilai_kepemimpinan,

                                            'nilai_total': nilai_total,
                                            'nilai_tpp': nilai_tpp,
                                        }
                                        skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
                                except:
                                    print "Some process cannot be calculate on calculate monthly";

                            #if is_can_calculate :
                            #    self.do_all_summary_calculation(cr,uid,employee_user_id,employee_id,param_year)
                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


        return {}

    def action_recalculate_by_company_yearly(self, cr, uid, ids, context={}):

        partner_pool = self.pool.get('res.partner')
        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                try :
                    param_year = param_obj.param_target_period_year
                    param_company_id  = param_obj.param_company_id.id
                    is_single_calc=False


                    if not param_obj.param_employee_id :
                        #print "all emp"
                        if not param_obj.param_all_company :
                            employee_ids=partner_pool.search(cr, uid, [('company_id','=',param_company_id),('employee','=',True)
                                                         ], context=None)
                        else :
                            employee_ids=partner_pool.search(cr, uid, [('employee','=',True)
                                                         ], context=None)
                        employee_objs = partner_pool.read(cr, uid, employee_ids, ['id','user_id','company_id'], context=context)
                    else :
                        is_single_calc=True;
                        employee_objs = [param_obj.param_employee_id,]
                        #print "1 emp"


                    for employee_obj in employee_objs:

                        try :
                            if not is_single_calc:
                                employee_id=employee_obj['id']
                                is_can_calculate=False
                                if employee_obj['user_id']:
                                    employee_user_id=employee_obj['user_id'][0]
                                    is_can_calculate=True
                                if employee_obj['company_id']:
                                    employee_company_id=employee_obj['company_id'][0]
                            else :
                                employee_id=employee_obj.id
                                is_can_calculate=False
                                if employee_obj.user_id:
                                    employee_user_id=employee_obj.user_id.id
                                    is_can_calculate=True
                                if employee_obj.company_id:
                                    employee_company_id=employee_obj.company_id.id




                            if is_can_calculate :
                                self.do_all_summary_calculation(cr,uid,employee_user_id,employee_id,param_year)
                        except:
                            print "Some process cannot be calculate on load person";
                except:
                    print "Some process cannot be calculate on loop param";


        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.skp.employee.process',
                'target': 'new',
                'context': context,
            }

    def do_all_summary_calculation(self, cr,uid, user_id,employee_id, target_period_year,  context=None):

        sey_pool = self.pool.get('skp.employee.yearly')
        data_skp_summary  = sey_pool._get_akumulasi_realiasai_per_bulan_opt(cr, user_id,employee_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id,
                            'user_id': user_id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)

        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):
                        count_of_month = data_skp_summary['count_of_month']
                        if data_skp_summary:
                            update_values = {
                                'jml_skp': data_skp_summary['jml_skp'],
                                'jml_skp_done': data_skp_summary['jml_skp_done'],
                                'jml_realisasi_skp':data_skp_summary['jml_realisasi_skp'],
                                'nilai_skp': data_skp_summary['nilai_skp']   ,
                                'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],

                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'fn_nilai_tambahan': data_skp_summary['fn_nilai_tambahan'],
                                'fn_nilai_kreatifitas': data_skp_summary['fn_nilai_kreatifitas'],

                                'jml_perilaku': data_skp_summary['jml_perilaku'],
                                'nilai_perilaku': data_skp_summary['nilai_perilaku'],
                                'nilai_perilaku_percent': data_skp_summary['nilai_perilaku_percent'],
                                'nilai_total': data_skp_summary['nilai_total'],
                                'nilai_pelayanan': data_skp_summary['nilai_pelayanan'],
                                'nilai_integritas': data_skp_summary['nilai_integritas'],
                                'nilai_komitmen': data_skp_summary['nilai_komitmen'],
                                'nilai_disiplin': data_skp_summary['nilai_disiplin'],
                                'nilai_kerjasama': data_skp_summary['nilai_kerjasama'],
                                'nilai_kepemimpinan': data_skp_summary['nilai_kepemimpinan'],
                                'indeks_nilai_pelayanan': data_skp_summary['indeks_nilai_pelayanan'],
                                'indeks_nilai_integritas': data_skp_summary['indeks_nilai_integritas'],
                                'indeks_nilai_komitmen': data_skp_summary['indeks_nilai_komitmen'],
                                'indeks_nilai_disiplin': data_skp_summary['indeks_nilai_disiplin'],
                                'indeks_nilai_kerjasama': data_skp_summary['indeks_nilai_kerjasama'],
                                'indeks_nilai_kepemimpinan': data_skp_summary['indeks_nilai_kepemimpinan'],
                                'indeks_nilai_total': data_skp_summary['indeks_nilai_total'],
                            }
                            sey_pool.write(cr , uid,[skp_yearly_obj.id,], update_values, context=None)
        return True;

skp_employee_settings()