from openerp.osv import fields, osv
from string import split

class notification_user_activation_done(osv.osv_memory):
    _name = "notification.user.activation.done"
    _columns = {
        'name': fields.char('Notif', size=128),
    }
notification_user_activation_done();
class app_skp_settings(osv.osv_memory):
    _name = 'app.skp.settings'
    _description = 'Konfigurasi Aplikasi SKP'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP SIPKD', size=64, readonly=True),
            'param_type'     : fields.selection([('all', 'Semua'), ('skp', 'SKP'),
                                                      ('perilaku', 'Perilaku'), ('tk', 'Tambahan Dan Kreatifitas')], 'Jenis Kegiatan'
                                                     ),
            'param_list_of_ids' : fields.char('Long Char', size=400, ),
            'param_target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'param_target_period_year'     : fields.char('Periode Tahun', size=4, ),
            'param_company_id' : fields.many2one('res.company','DITJEN')
    }
    def action_recalculate_by_ids(self, cr, uid, ids, context={}):

        skp_employee_pool = self.pool.get('skp.employee')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids = split(param_obj.param_list_of_ids,',')
                skp_emp_ids=[]
                for an_id in data_ids:
                    skp_emp_ids.append(int(an_id))
                for skp_employee_obj in  skp_employee_pool.browse(cr, uid, skp_emp_ids, context=context):
                        if param_obj.param_type=='skp':
                            skp_state_count = skp_employee_obj.skp_state_count;
                            jml_skp = skp_employee_obj.jml_skp;
                            jml_all_skp = skp_employee_obj.jml_all_skp;
                            nilai_skp = skp_employee_obj.nilai_skp;
                            nilai_skp_percent = skp_employee_obj.nilai_skp_percent;
                            nilai_skp_tambahan_percent = skp_employee_obj.nilai_skp_tambahan_percent
                            nilai_total = skp_employee_obj.nilai_total;
                            nilai_tpp = skp_employee_obj.nilai_tpp;
                        
                            update_values = {
                                'skp_state_count': skp_state_count,
                                'jml_skp': jml_skp,
                                'jml_all_skp': jml_all_skp,
                                'nilai_skp': nilai_skp,
                                'nilai_skp_percent': nilai_skp_percent,
                                'nilai_skp_tambahan_percent': nilai_skp_tambahan_percent,
                                'nilai_total': nilai_total,
                                'nilai_tpp': nilai_tpp,
                            }
                            skp_employee_pool.write(cr , uid,[skp_employee_obj.id,], update_values, context=None)
        
        return {}
    def activate_all_users(self, cr, uid,ids, context=None):
        
        cr.execute('update res_users u set active=%s from res_partner p where p.user_id = u.id and p.employee ', (True,))
        
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.user.activation.done',
                'target': 'new',
                'context': context,
            }
    def deactivate_all_users(self, cr, uid,ids, context=None):
        
        cr.execute('update res_users u set active=%s from res_partner p where p.user_id = u.id and p.employee ', (False,))
        
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.user.activation.done',
                'target': 'new',
                'context': context,
            }

    def activate_all_users_opd(self, cr, uid,ids, context=None):
        
        for param_obj in self.browse(cr, uid, ids, context=context):
            if param_obj.param_company_id:
                company_id = param_obj.param_company_id.id
                cr.execute('update res_users u set active=%s from res_partner p where p.user_id = u.id and u.company_id=%s and p.employee ', (True,company_id))
        
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.user.activation.done',
                'target': 'new',
                'context': context,
            }
    def deactivate_all_users_opd(self, cr, uid,ids, context=None):
        
        for param_obj in self.browse(cr, uid, ids, context=context):
            if param_obj.param_company_id:
                company_id = param_obj.param_company_id.id
                cr.execute('update res_users u set active=%s from res_partner p where p.user_id = u.id and u.company_id=%s and p.employee ', (False,company_id))
        
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.user.activation.done',
                'target': 'new',
                'context': context,
            }
app_skp_settings()