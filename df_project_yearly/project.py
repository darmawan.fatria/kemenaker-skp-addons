# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class project(osv.osv):
    _inherit = 'project.project'
    _columns = {
        'realisasi_jumlah_kuantitas_output'     : fields.float('Kuantitas Output', digits_compute=dp.get_precision('no_digit'),),
        'realisasi_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output'),
        'realisasi_angka_kredit'                : fields.float('Angka Kredit', digits_compute=dp.get_precision('angka_kredit')),
        'realisasi_kualitas'                    : fields.float('Kualitas', digits_compute=dp.get_precision('no_digit')),
        'realisasi_waktu'                       : fields.integer('Waktu',),
        'realisasi_satuan_waktu'                : fields.selection([('bulan', 'Bulan')], 'Satuan Waktu'),
        'realisasi_biaya'                       : fields.float('Biaya'),
        
        'suggest_jumlah_kuantitas_output'     : fields.float('Kuantitas Output', digits_compute=dp.get_precision('no_digit'),),
        'suggest_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output'),
        'suggest_angka_kredit'                : fields.float('Angka Kredit', digits_compute=dp.get_precision('angka_kredit')),
        'suggest_kualitas'                    : fields.float('Kualitas', digits_compute=dp.get_precision('no_digit')),
        'suggest_waktu'                       : fields.integer('Waktu',),
        'suggest_satuan_waktu'                : fields.selection([('bulan', 'Bulan'), ('hari', 'Hari')], 'Satuan Waktu'),
        'suggest_biaya'                       : fields.float('Biaya'),
        
        
        'appeal_jumlah_kuantitas_output'     : fields.float('Kuantitas Output', digits_compute=dp.get_precision('no_digit'),),
        'appeal_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output'),
        'appeal_angka_kredit'                : fields.float('Angka Kredit', digits_compute=dp.get_precision('angka_kredit')),
        'appeal_kualitas'                    : fields.float('Kualitas', digits_compute=dp.get_precision('no_digit')),
        'appeal_waktu'                       : fields.integer('Waktu',),
        'appeal_satuan_waktu'                : fields.selection([('bulan', 'Bulan')], 'Satuan Waktu'),
        'appeal_biaya'                       : fields.float('Biaya'),
        
        
        'work_state': fields.selection([('draft', 'Draft'), ('realisasi', 'Realisasi'),
                                        ('propose', 'Atasan'), ('rejected_manager', 'Pengajuan Ditolak Atasan'),
                                        ('appeal', 'Banding'), ('evaluated', 'Verifikatur'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                        ('propose_to_close', 'Pengajuan Closing Target'), ('closed', 'Closed'),
                                        ('done', 'Selesai'), ('cancelled', 'Cancel')], 'Status Pekerjaan'),
        'is_suggest': fields.boolean('Tambahkan Koreski Penilaian'),
        'is_appeal': fields.boolean('Tambahkan Koreski Banding'),
        'is_control': fields.boolean('Verifikasi Penilaian'),
        'is_verificated': fields.boolean('Verifikasi Diterima'),
        'notes_atasan'      : fields.text('Catatan Koreksi Pejabat Penilai', readolny=True),
        'notes_atasan_banding'      : fields.text('Catatan Koreksi Atasan Pejabat Penilai' , readolny=True),
        'notes_from_bkd'     : fields.text('Catatan Petugas Verifikasi',),
         'use_target_for_calculation': fields.boolean('Hitung Menggunakan Target'),
        'control_count': fields.integer('Jumlah Pengajuan Verifikasi', readonly=True),
        'nilai_akhir': fields.float('Nilai', readonly=True),
        'jumlah_perhitungan': fields.float('Jumlah Perhitungan', readonly=True),
        'indeks_nilai': fields.char('Indeks', size=20, readonly=True),
        
        'target_realiasi_notsame': fields.boolean('Realisasi Tidak Sama Target', help="Realisasi Tidak Sama Target"),
        'biaya_verifikasi_notsame': fields.boolean('Status Verifikasi Biaya', help="Realisasi Tidak Sama Target"),
    }
    _defaults = {
        'work_state': 'realisasi',
        'is_suggest': False,
        'is_appeal': False,
        'is_control': False,
        'is_verificated': False,
        'use_target_for_calculation':False,
        }
    
    def set_propose_yearly(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'work_state':'propose'}, context=context)
    def set_evaluated_yearly(self, cr, uid, ids, context=None):

        for task_id in ids :
            self.write(cr, uid, task_id, {'target_realiasi_notsame':self.is_status_target_realiasi_notsame(cr, uid, task_id, context),
                                      'biaya_verifikasi_notsame':self.is_status_biaya_verifikasi_notsame(cr, uid, task_id, context),
                                    }, context=context)
        self.write(cr, uid, ids, {'work_state':'evaluated', },)
        for target_tahunan in self.browse(cr, uid, ids, context=context):
            self.do_skp_summary_calculation(cr,uid,target_tahunan.user_id,target_tahunan.employee_id,target_tahunan.target_period_year)

        return True
    def set_appeal(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'work_state':'appeal'}, context=context)
    def set_done(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'work_state':'done'}, context=context)
        for target_tahunan in self.browse(cr, uid, ids, context=context):
            self.do_skp_summary_calculation(cr,uid,target_tahunan.user_id,target_tahunan.employee_id,target_tahunan.target_period_year)


        return True;
    def action_cancelled_yearly(self, cr, uid, ids, context=None):
        task_pool = self.pool.get('project.skp')
        for project_id in ids :
            task_ids = task_pool.search(cr, 1, [('project_id', '=', project_id)], context=context);
            task_pool.write(cr, 1, task_ids, {'work_state':'cancelled',
                                    }, context=context)
        return self.write(cr, 1, ids, {'work_state':'cancelled',
                                       'state':'cancelled'}, context=context)
    def action_realisasi(self, cr, uid, ids, context=None):
        """ Selesai mengerjakan input realisasi, 
        """
        if self.is_user_realiasi(cr, uid, ids, context) :
            if self.validate_ajukan_atasan_state(cr, uid, ids, context) :
                return self.set_propose_yearly(cr, uid, ids, context=context)
    
    def action_propose(self, cr, uid, ids, context=None):
        """ Ajukan Verifikasi
        """
        if self.is_manager_realiasi(cr, uid, ids, context) :
            return self.set_evaluated_yearly(cr, uid, ids, context=context)
    def action_propose_rejected_popup(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_yearly', 'action_yearly_propose_rejected_popup_form_view')

        if self.is_manager_realiasi(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            return {
                'name':_("Pengajuan Di Tolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.yearly.propose.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_jumlah_kuantitas_output': task_obj.realisasi_jumlah_kuantitas_output,
                    'default_satuan_kuantitas_output':task_obj.realisasi_satuan_kuantitas_output.id,
                    'default_kualitas':task_obj.realisasi_kualitas ,
                    'default_waktu': task_obj.realisasi_waktu,
                    'default_satuan_waktu': task_obj.realisasi_satuan_waktu,
                    'default_biaya': task_obj.realisasi_biaya,
                    'default_angka_kredit': task_obj.realisasi_angka_kredit,
                    'default_is_suggest': True,
                    'default_project_id':task_obj.id
                }
            }
        return False
    def action_appeal(self, cr, uid, ids, context=None):
        """ Penolakan di Banding ke Atasan 
        """
        if self.is_user_realiasi(cr, uid, ids, context) :
            self.fill_task_appeal_automatically_with_suggest(cr, uid, ids, context);
            return self.set_appeal(cr, uid, ids, context=context)
    def action_dont_appeal(self, cr, uid, ids, context=None):
        """ Penolakan Diterima, Langsung ajukan Verifikasi,
        """
        if self.is_user_realiasi(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated_yearly(cr, uid, ids, context=context)
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        if self.is_verificator_realiasi(cr, uid, ids, context) :
            self.do_task_poin_calculation(cr, uid, ids, context=context)
            ##self.do_task_summary_calculation(cr, uid, ids, context=context)
            return self.set_done(cr, uid, ids, context=context)
    def action_appeal_approve(self, cr, uid, ids, context=None):
        """ Banding DIterima, Langsung ajukan Verifikasi,
        """
        if self.is_appeal_manager_realiasi(cr, uid, ids, context) : 
            return self.set_evaluated_yearly(cr, uid, ids, context=context)
    def action_appeal_reject_popup(self, cr, uid, ids, context=None):
        """ Banding Ditolak
        """
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_yearly', 'action_yearly_appeal_rejected_popup_form_view')
        

        if self.is_appeal_manager_realiasi(cr, uid, ids, context):
            task_obj = self.browse(cr, uid, ids[0], context=context)
            return {
                'name':_("Pengajuan Banding Di Tolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.yearly.appeal.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_jumlah_kuantitas_output': task_obj.suggest_jumlah_kuantitas_output,
                    'default_satuan_kuantitas_output':task_obj.suggest_satuan_kuantitas_output.id,
                    'default_kualitas':task_obj.suggest_kualitas ,
                    'default_waktu': task_obj.suggest_waktu,
                    'default_satuan_waktu': task_obj.suggest_satuan_waktu,
                    'default_biaya': task_obj.suggest_biaya,
                    'default_angka_kredit': task_obj.suggest_angka_kredit,
                    'default_is_appeal': True,
                    'default_project_id':task_obj.id
    
                }
            }
        return False
    def do_recalculate_poin(self, cr, uid, ids, context=None):
         if self.is_verificator_realiasi(cr, uid, ids, context) :
            update_poin = {             'nilai_akhir': 0,
                                        'indeks_nilai': False,
                                        'jumlah_perhitungan':0,
                                        'use_target_for_calculation': False,
                                         }
            self.write(cr, uid, ids, update_poin)
            return self.set_evaluated_yearly(cr, uid, ids, context=context)
                 
         return True;
    def action_done_use_target(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        if self.is_verificator_realiasi(cr, uid, ids, context) :
            use_target = {'use_target_for_calculation': True, }
            self.write(cr, uid, ids, use_target, context=context)
            self.do_task_poin_calculation(cr, uid, ids, context=context)
            return self.set_done(cr, uid, ids, context=context)
    
    def action_work_rejected(self, cr, uid, ids, context=None):
        """ Verifikasi Ditolak
        """
        
        if not ids: return []
        
        if self.is_verificator_realiasi(cr, uid, ids, context) :
            dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_project_yearly', 'action_yearly_verificate_rejected_popup_form_view')
            task_obj = self.browse(cr, uid, ids[0], context=context)
            if task_obj.control_count >= 2 :
                raise osv.except_osv(_('Invalid Action, Limit Action'),
                                         _('Hasil Verifikasi Tidak Dapat Ditolak, Karena Sudah Dilakukan Pengajuan Dan Verifikasi Sebanyak 2 Kali.'))
            return {
                'name':_("Verifikasi Ditolak"),
                'view_mode': 'form',
                'view_id': view_id,
                'view_type': 'form',
                'res_model': 'project.skp.yearly.verificate.rejected',
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'domain': '[]',
                'context': {
                    'default_control_count':task_obj.control_count + 1,
                    'default_is_control': True,
                    'default_project_id':task_obj.id
    
                }
            }
        return False
    def action_verificate_revision(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        if self.is_manager_realiasi(cr, uid, ids, context) :
            self.fill_task_automatically_with_suggest(cr, uid, ids, context);
            return self.set_evaluated_yearly(cr, uid, ids, context=context)
    # perhitungan:
    def do_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        update_poin = self.prepare_task_poin_calculation(cr, uid, ids, context=context)
        if update_poin:
            self.write(cr, uid, ids, update_poin)
            
        return True
    def prepare_task_poin_calculation(self, cr, uid, ids, context=None):
        """ BKD->Done (Keuangan) """
        # print "Poin Calculation"
        lookup_nilai_pool = self.pool.get('acuan.penailaian')
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            # close task...
            nilai_akhir = 0
            jumlah_perhitungan = 0
            pengali_waktu_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_waktu'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
            pengali_waktu = self.pool.get('config.skp').browse(cr, uid, pengali_waktu_ids)[0].config_value_float
            pengali_biaya_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_biaya'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
            pengali_biaya = self.pool.get('config.skp').browse(cr, uid, pengali_biaya_ids)[0].config_value_float
            efektifitas_biaya_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_std_b'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
            efektifitas_biaya = self.pool.get('config.skp').browse(cr, uid, efektifitas_biaya_ids)[0].config_value_int or 0
            efektifitas_waktu_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'x_std_w'), ('active', '=', True), ('type', '=', 'nilai')], context=None)
            efektifitas_waktu = self.pool.get('config.skp').browse(cr, uid, efektifitas_waktu_ids)[0].config_value_int or 0
            
            employee = task_obj.employee_id
            # print " pengali_waktu : ", pengali_waktu, " | pengali_biaya : ", pengali_biaya
            if not employee :
                 raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena User Login Belum Dikaitkan Dengan Data Pegawai.'))
            else :
                job_type = employee.job_type
                if not job_type :
                     raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Jenis Jabatan Pegawai Belum Diisi, Harap Dilengkapi Terlebih Dahulu Di Data Pegawai, Atau Data Jabatan.'))
            if task_obj:
                if task_obj.target_type_id in ('dipa_apbn', 'dpa_opd_biro', 'sotk', 'lain_lain'):
                    # validator Project
                    pembagi = 0
                    a = b = c = d = e = 0
                    if job_type in ('struktural', 'jft', 'jfu') :
                        if task_obj.target_jumlah_kuantitas_output != 0:
                            x = float (self.get_value_realisasi_or_target(task_obj.target_jumlah_kuantitas_output,
                                                                          task_obj.realisasi_jumlah_kuantitas_output,
                                                                          task_obj.use_target_for_calculation)
                                       )
                            y = float (task_obj.target_jumlah_kuantitas_output)
                            a = (x / y) * 100
                            if a > 100 :
                                a = 100
                            pembagi += 1
                            
                        elif task_obj.target_jumlah_kuantitas_output == 0 and task_obj.realisasi_jumlah_kuantitas_output > 0 :
                             a = 100
                             pembagi += 1
                        if task_obj.target_kualitas != 0 :
                            b = (float(self.get_value_realisasi_or_target(task_obj.target_kualitas,
                                                                          task_obj.realisasi_kualitas,
                                                                          task_obj.use_target_for_calculation) 
                                       / task_obj.target_kualitas)) * 100
                            if b > 100 :
                                b = 100
                            pembagi += 1
                        elif task_obj.target_kualitas == 0 and task_obj.realisasi_kualitas > 0 :
                            b = 100
                            pembagi += 1
                        if task_obj.target_waktu > 0:
                            x = float (self.get_value_realisasi_or_target(task_obj.target_waktu,
                                                                          task_obj.realisasi_waktu,
                                                                          task_obj.use_target_for_calculation))
                            y = float (task_obj.target_waktu)
                            x_efektifitas = 100 - ((x / y) * 100)
                            c = 0
                           # if x_efektifitas == 0.0 :
                            #        c = (x / y) * 100
                            if  x_efektifitas <= 24.0 :
                                    c = ((pengali_waktu * y - x) / y) * 100
                            if  x_efektifitas > 24.0 :
                                    c = ((pengali_waktu * y - x) / y) * 100
                                    c = 76 - (c - 100)
                            if c > 100 :
                                c = 100
                            pembagi += 1
                    if job_type in ('struktural') :
                        if task_obj.target_biaya > 0:
                            x = float (self.get_value_realisasi_or_target(task_obj.target_biaya,
                                                                          task_obj.realisasi_biaya,
                                                                          task_obj.use_target_for_calculation))
                            y = float (task_obj.target_biaya)
                            d = 0
                            if task_obj.target_type_id in ('dipa_apbn', 'dpa_opd_biro', 'sotk'):  # untuk Pengeluaran
                                x_efektifitas = 100 - ((x / y) * 100)
                                if  x_efektifitas <= 24.0 :
                                        d = ((pengali_biaya * y - x) / y) * 100
                                if  x_efektifitas > 24.0 :
                                        d = ((pengali_biaya * y - x) / y) * 100
                                        d = 76 - (d - 100)
                            else :  # untuk penerimaan
                                d = (x / y) * 100
                            
                            if d > 100 :
                                d = 100
                            pembagi += 1
                            
                    if job_type in ('jft') :
                        if task_obj.target_angka_kredit != 0:
                            x = float (self.get_value_realisasi_or_target(task_obj.target_angka_kredit,
                                                                          task_obj.realisasi_angka_kredit,
                                                                          task_obj.use_target_for_calculation))
                            y = float (task_obj.target_angka_kredit)
                            e = (x / y) * 100
                            if e > 100 :
                                e = 100
                            pembagi += 1
                        elif task_obj.target_angka_kredit == 0  and task_obj.realisasi_angka_kredit > 0 :
                             e = 100
                             pembagi += 1
                            
                    
                    # print " Calc : ", a, " ,", b, " ,", c, " ,", d, " ,", e , " = ", (a + b + c + d + e)
                    if pembagi > 0 :
                        jumlah_perhitungan = (a + b + c + d + e)
                        nilai_akhir = (jumlah_perhitungan / pembagi)
                        if nilai_akhir > 100:
                            nilai_akhir = 100;
            update_poin = {
                                'nilai_akhir': nilai_akhir,
                                'jumlah_perhitungan':jumlah_perhitungan,
                                 }
            return update_poin;
            
        return False
    # VALIDATOR
    def get_realisasi_auth_id(self, cr, uid, ids, type, context=None):
        if not isinstance(ids, list): ids = [ids]
        for task in self.browse(cr, uid, ids, context=context):
            
            if type == 'user_id' :
                if task.user_id :
                    if task.user_id.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_atasan' :
                if task.user_id_atasan :
                    if task.user_id_atasan.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_banding' :
                if task.user_id_banding :
                    if task.user_id_banding.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type == 'user_id_bkd' :
                if task.user_id_bkd :
                    if task.user_id_bkd.id != uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
                
            else : 
               return False;
            
        return True;
    def get_value_realisasi_or_target(self, target, realisasi, use_target_for_calculation):
        if use_target_for_calculation : 
           return target
        return realisasi
    def is_user_realiasi(self, cr, uid, ids, context=None):
        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_realisasi_auth_id(cr, uid, [target_id], 'user_id', context=context) 
    def is_manager_realiasi(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        return self.get_realisasi_auth_id(cr, uid, [task_id], 'user_id_atasan', context=context) 
    def is_appeal_manager_realiasi(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        return self.get_realisasi_auth_id(cr, uid, [task_id], 'user_id_banding', context=context) 
    def is_verificator_realiasi(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        return self.get_realisasi_auth_id(cr, uid, [task_id], 'user_id_bkd', context=context)
    def is_user_or_is_verificator(self, cr, uid, ids, context=None):
        task_id = len(ids) and ids[0] or False
        if not task_id: return False
        accepted = self.get_realisasi_auth_id(cr, uid, [task_id], 'user_id', context=context) or self.get_auth_id(cr, uid, [task_id], 'user_id_bkd', context=context)
        return accepted
    
    def validate_ajukan_atasan_state(self, cr, uid, ids, context=None):
         for target_obj in self.browse(cr, uid, ids, context=context):
                 if target_obj.state in ('confirm', 'closed'):
                    return True;
                 else :
                     raise osv.except_osv(_('Invalid Action, Proses Tidak Dapat Dilanjutkan'),
                                        _('Proses Tidak Dapat Dilanjutkan Karena Target Belum Di Terima.'))
         return True;
     # automatic fill
    def fill_task_appeal_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            
            if task_obj.is_suggest:
                vals.update({
                                    'appeal_jumlah_kuantitas_output'     : task_obj.suggest_jumlah_kuantitas_output,
                                    'appeal_satuan_kuantitas_output'     : task_obj.suggest_satuan_kuantitas_output.id or None,
                                    'appeal_angka_kredit'     : task_obj.suggest_angka_kredit,
                                    'appeal_kualitas'     : task_obj.suggest_kualitas,
                                    'appeal_waktu'     : task_obj.suggest_waktu,
                                    'appeal_satuan_waktu'     : task_obj.suggest_satuan_waktu,
                                    'appeal_biaya'     : task_obj.suggest_biaya,
                                })
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_suggest(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj.is_suggest:
                vals.update({
                                    'realisasi_jumlah_kuantitas_output'     : task_obj.suggest_jumlah_kuantitas_output,
                                    'realisasi_satuan_kuantitas_output'     : task_obj.suggest_satuan_kuantitas_output.id or None,
                                    'realisasi_angka_kredit'                : task_obj.suggest_angka_kredit,
                                    'realisasi_kualitas'                    : task_obj.suggest_kualitas,
                                    'realisasi_waktu'                       : task_obj.suggest_waktu,
                                    'realisasi_satuan_waktu'                : task_obj.suggest_satuan_waktu,
                                    'realisasi_biaya'                       : task_obj.suggest_biaya,
                                })
                        
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    def fill_task_automatically_with_appeal(self, cr, uid, ids, context=None):
        """ Jika selesai mebuat target. Maka secara default realisasi akan otomatis terisi dengan nilai target tersebit"""
        if not isinstance(ids, list): ids = [ids]
        for task_obj in self.browse(cr, uid, ids, context=context):
            vals = {}
            if task_obj.is_appeal:
                vals.update({
                                    'realisasi_jumlah_kuantitas_output'     : task_obj.appeal_jumlah_kuantitas_output,
                                    'realisasi_satuan_kuantitas_output'     : task_obj.appeal_satuan_kuantitas_output.id or None,
                                    'realisasi_angka_kredit'                : task_obj.appeal_angka_kredit,
                                    'realisasi_kualitas'                    : task_obj.appeal_kualitas,
                                    'realisasi_waktu'                       : task_obj.appeal_waktu,
                                    'realisasi_satuan_waktu'                : task_obj.appeal_satuan_waktu,
                                    'realisasi_biaya'                       : task_obj.appeal_biaya,
                                })
                              
                # end if               
                self.write(cr, uid, [task_obj.id], vals, context)
        # end for
        return True
    # Optimasi Fungsu :
    def is_status_target_realiasi_notsame(self, cr, uid, task_id, context=None):
        
        
        if not task_id:
            return False
        target_realiasi_notsame = False
        task_obj = self.browse(cr, uid, task_id, context=None)
        if task_obj:
                 if task_obj.target_jumlah_kuantitas_output > task_obj.realisasi_jumlah_kuantitas_output :
                    target_realiasi_notsame = True
                 if task_obj.target_kualitas != 0 and task_obj.target_kualitas > task_obj.realisasi_kualitas :
                    target_realiasi_notsame = True
                 if task_obj.target_waktu > task_obj.realisasi_waktu :
                    target_realiasi_notsame = True
                 if task_obj.target_biaya > task_obj.realisasi_biaya and target_realiasi_notsame:
                    target_realiasi_notsame = True
                 return target_realiasi_notsame
                 
        return target_realiasi_notsame 
    
    def is_status_biaya_verifikasi_notsame(self, cr, uid, task_id, context=None):
        if not task_id:
            return False
        task_obj = self.browse(cr, uid, task_id, context=None)
        if task_obj:
                if  (task_obj.target_jumlah_kuantitas_output <= task_obj.realisasi_jumlah_kuantitas_output and \
                        task_obj.target_kualitas <= task_obj.realisasi_kualitas and \
                        task_obj.target_waktu <= task_obj.realisasi_waktu) and \
                        task_obj.realisasi_biaya != task_obj.target_biaya :
                        return True
        return False
    def do_skp_summary_calculation(self, cr,uid, user_id,employee_id, target_period_year,  context=None):

        sey_pool = self.pool.get('skp.employee.yearly')
        data_skp_summary  = sey_pool._get_akumulasi_realisasi_skp_tambahan_per_bulan(cr, user_id,employee_id, target_period_year, context=context)
        skp_yearly_ids = sey_pool.search(cr, uid, [('employee_id', '=', employee_id.id),
                                                     ('target_period_year', '=', target_period_year),
                                                   ], context=None)
        if not skp_yearly_ids:
                        values = {
                            'employee_id': employee_id.id,
                            'user_id': user_id.id,
                            'target_period_year': target_period_year,
                        }
                        new_skp_yearly_id=sey_pool.create(cr , uid, values, context=None)
                        skp_yearly_ids.append(new_skp_yearly_id)

        if data_skp_summary and skp_yearly_ids :
            for skp_yearly_obj in  sey_pool.browse(cr, uid, skp_yearly_ids, context=context):

                        nilai_total = skp_yearly_obj.nilai_perilaku_percent + data_skp_summary['nilai_skp_tambahan_percent']
                        indeks_nilai_total = sey_pool.get_indeks_nilai(cr,uid,nilai_total,context=None)
                        if data_skp_summary:
                            update_values = {
                                'jml_skp': data_skp_summary['jml_skp'],
                                'jml_skp_done': data_skp_summary['jml_skp_done'],
                                'jml_realisasi_skp':data_skp_summary['jml_realisasi_skp'],
                                'nilai_skp': data_skp_summary['nilai_skp']   ,
                                'nilai_skp_tambahan': data_skp_summary['nilai_skp_tambahan']   ,
                                'nilai_skp_percent': data_skp_summary['nilai_skp_percent'],
                                'nilai_skp_tambahan_percent': data_skp_summary['nilai_skp_tambahan_percent'],
                                'indeks_nilai_skp': data_skp_summary['indeks_nilai_skp'],
                                'indeks_nilai_total': indeks_nilai_total,
                                'nilai_total': round(nilai_total,2),
                            }
                            sey_pool.write(cr , uid,[skp_yearly_obj.id,], update_values, context=None)
        return True;
project()


