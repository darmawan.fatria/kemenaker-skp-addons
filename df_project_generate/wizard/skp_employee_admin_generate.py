from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time
from mx import DateTime

class skp_employee_admin_generate(osv.Model):
    _name = "skp.employee.admin.generate"
    _description = "Generate Template SKP EMPLOYEE By Admin Bulanan"
    
    _columns = {
        'company_id': fields.many2one('res.company', 'DITJEN', ),
        'lama_kegiatan'     : fields.integer('Lama Kegiatan',readonly=False),
        'target_period_month'      : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                ('03', 'Maret'), ('04', 'April'),
                                                ('05', 'Mei'), ('06', 'Juni'),
                                                ('07', 'Juli'), ('08', 'Agustus'),
                                                ('09', 'September'), ('10', 'Oktober'),
                                                ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                ,required=True ),
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        }
    _defaults = {
        'lama_kegiatan' : 1,
        'target_period_year':lambda *args: time.strftime('%Y'),
       
        }
    def scheduler_generate_skp_employee_per_month(self, cr, uid,param_1,param_2, context=None):
        if context is None:
            context = {}
        values =  {}
        print "executer scheduler_generate_skp_employee_per_month "
        skp_employee_pool = self.pool.get('skp.employee')
        user_pool = self.pool.get('res.users')
        employee_pool = self.pool.get('res.partner')
        company_pool = self.pool.get('res.company')
        if param_1 and  param_2:
            company_ids = company_pool.search(cr,uid,[('employee_id_kepala_instansi','!=',False)],context=None);
            print "company_ids_to_generate : ",company_ids
            for company_id in company_ids :
                employee_ids = employee_pool.search(cr, uid, [('company_id','=',company_id),('employee','=',True)], context=None)
                for employee in  employee_pool.browse(cr,uid,employee_ids,context=None):
                    #Init Field
                    user_id = employee.user_id.id
                    description=''
                    lama_kegiatan=1
                    target_period_year =param_2
                    target_period_month=param_1
                    if not employee :
                        continue;
                    else :
                        company = employee.company_id
                        company_id = company.id
                        if not employee.employee:
                            continue;
                        if not company_id :
                            continue;
                    employee_job_type = employee.job_type
                    employee_id = employee.id
                    values = {
                            'employee_id': employee_id,
                            'user_id': user_id,
                            'target_period_year':target_period_year,
                            'target_period_month':target_period_month,
                        }
                    #Check Duplicate Do Not Create
                    skp_emp_ids = skp_employee_pool.search(cr, uid, [('employee_id','=',employee_id),
                                                                          ('target_period_month','=',target_period_month),
                                                                          ('target_period_year','=',target_period_year),
                                                            ], context=None)
                    if skp_emp_ids:
                        continue;
                    #insert skp_employee    
                    new_skp_employee_id = skp_employee_pool.create(cr , uid, values, context=None)
                    
    def generate_skp_employee_per_month(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        values =  {}
        skp_employee_pool = self.pool.get('skp.employee')
        user_pool = self.pool.get('res.users')
        employee_pool = self.pool.get('res.partner')
        company_pool = self.pool.get('res.company')
        for task_generate in self.browse(cr, uid, ids, context=context):
            company_ids = []
            if task_generate.company_id :
                company_ids.append(task_generate.company_id.id)
            else :
                company_ids = company_pool.search(cr,uid,[('employee_id_kepala_instansi','!=',False)],context=None);
            print "company_ids_to_generate : ",company_ids
            for company_id in company_ids :
                employee_ids = employee_pool.search(cr, uid, [('company_id','=',company_id),('employee','=',True)], context=None)
                for employee in  employee_pool.browse(cr,uid,employee_ids,context=None):
                    #check Duplicate
                    #Init Field
                    user_id = employee.user_id.id
                    description=''
                    lama_kegiatan=task_generate.lama_kegiatan
                    target_period_year = task_generate.target_period_year
                    target_period_month=task_generate.target_period_month
                    if not employee :
                        continue;
                    else :
                        company = employee.company_id
                        company_id = company.id
                        if not employee.employee:
                            continue;
                        if not company_id :
                            continue;
                    employee_job_type = employee.job_type
                    employee_id = employee.id
                    values = {
                            'employee_id': employee_id,
                            'user_id': user_id,
                            'target_period_year':target_period_year,
                            'target_period_month':target_period_month,
                        }
                    #Check Duplicate Do Not Create
                    skp_emp_ids = skp_employee_pool.search(cr, uid, [('employee_id','=',employee_id),
                                                                          ('target_period_month','=',target_period_month),
                                                                          ('target_period_year','=',target_period_year),
                                                            ], context=None)
                    if skp_emp_ids:
                        continue;
                    #insert skp_employee    
                    new_skp_employee_id = skp_employee_pool.create(cr , uid, values, context=None)
                    
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.message.perilaku',
                'target': 'new',
                'context': {
                    'default_message_info': 'Berhasil Digenerate',
                    }
            }
    def generate_skp_employee_by_company(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        values =  {}
        skp_employee_pool = self.pool.get('skp.employee')
        user_pool = self.pool.get('res.users')
        company_pool = self.pool.get('res.company')
        for task_generate in self.browse(cr, uid, ids, context=context):
            company_ids = []
            if task_generate.company_id :
                company_ids.append(task_generate.company_id.id)
            else :
                company_ids = company_pool.search(cr,uid,[('employee_id_kepala_instansi','!=',False)],context=None);
            for company_id in company_ids :
                user_ids = user_pool.search(cr, uid, [('company_id','=',company_id)], context=None)
                
                for user_obj in  user_pool.browse(cr,uid,user_ids,context=None):
                    #check Duplicate
                    #Init Field
                    user_id = user_obj.id
                    description=''
                    lama_kegiatan=task_generate.lama_kegiatan
                    target_period_year = task_generate.target_period_year
                    target_period_month='xx'
                    employee = user_obj.partner_id
                    
                    if not employee :
                        continue;
                    else :
                        company = employee.company_id
                        company_id = company.id
                        if not employee.employee:
                            continue;
                         
                        if not company_id :
                            continue;
                    employee_job_type = employee.job_type
                    employee_id = employee.id
                    
                    values = {
                            'employee_id': employee_id,
                            'user_id': user_id,
                            'target_period_year':target_period_year,
                            
                        }
                    #Update Task Target Bulanan
                    now=DateTime.today();
                    first_task_id=None
                    january=DateTime.Date(now.year,1,1)
                    curr_date =  DateTime.strptime(january.strftime('%Y-%m-%d'),'%Y-%m-%d')
                    
                    first_date =curr_date
                    for i in range(0,lama_kegiatan):
                            next_date = curr_date + DateTime.RelativeDateTime(months=i)
                            target_period_month=next_date.strftime('%m')
                            values.update({
                                       'target_period_month':target_period_month,
                             })
                            #Check Duplicate Do Not Create
                            skp_emp_ids = skp_employee_pool.search(cr, uid, [('employee_id','=',employee_id),
                                                                          ('target_period_month','=',target_period_month),
                                                                          ('target_period_year','=',target_period_year),
                                                            ], context=None)
                            if skp_emp_ids:
                                continue;
                            date_start=first_date
                            date_end=first_date
                            #insert task
                            new_skp_employee_id = skp_employee_pool.create(cr , uid, values, context=None)
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.message.perilaku',
                'target': 'new',
                'context': {
                    'default_message_info': 'Berhasil Digenerate',
                    }
            }
        
   
skp_employee_admin_generate()