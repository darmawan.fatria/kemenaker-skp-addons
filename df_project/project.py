from datetime import datetime, date
from lxml import etree
import time
from mx import DateTime

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import netsvc
from openerp import workflow
import openerp.addons.decimal_precision as dp

class project(osv.Model):
    _name = "project.project"
    _description = "Target SKP Tahunan"

    _inherit = ['mail.thread', 'ir.needaction_mixin']
    
    def write(self, cr, uid, ids, vals, context=None):
        if not isinstance(ids, list): ids = [ids]
        for project_obj in self.read(cr, uid, ids, ['id','state'],context=context):
            target_id = project_obj['id']
            if uid != SUPERUSER_ID:
                if project_obj['state'] in ('template','draft','new','rejected_manager','correction'):
                    if not self.is_user(cr, uid, [target_id], context) :
                        return False
                if project_obj['state'] in ('propose','rejected_bkd'):
                    if not self.is_manager(cr, uid, [target_id], context) :
                        return False
                if project_obj['state'] in ('evaluated','propose_correction','propose_to_close','propose_cancel_close'):
                    if not self.is_verificator(cr,uid, [target_id], context) :
                        return False
                if project_obj['state'] in ('done','cancelled','open','pending'):
                    if not self.is_verificator(cr, uid, [target_id], context) :
                        return False
            
            if vals and vals.get('target_type_id',False):
                target_type_id = vals.get('target_type_id',False)
                if target_type_id : 
                    task_pool = self.pool.get('project.skp')
                    task_ids = task_pool.search(cr, SUPERUSER_ID, [('project_id','=',target_id)], context=None)
                    task_pool.write(cr, SUPERUSER_ID, task_ids, {'target_type_id':target_type_id}, context=None) 
                
        super(project, self).write(cr, uid, ids, vals, context=context)           
        return True
    
    def _get_total_target_biaya_(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.target_biaya >0 :
                if _target.realisasi_lines:
                    for task_obj in _target.realisasi_lines:
                        res[_target.id] += task_obj.target_biaya
                
        return res
    def _get_total_target_angka_kredit(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = 0.0
            if _target.target_angka_kredit >0 :
                if _target.realisasi_lines:
                    for task_obj in _target.realisasi_lines:
                        res[_target.id] += task_obj.target_angka_kredit
                
        return res
    def onchange_lama_kegiatan(self, cr, uid, ids, lama_kegiatan, context=None):
        res = {'value': {'target_waktu': 0,
                         }}
        if lama_kegiatan:
            res['value']['target_waktu'] =lama_kegiatan
        return res
    
    def _is_realisasi_ko_valid(self, cr, uid, ids, name, args, context, where =''):
        res = {}
        if not ids:
            return res
        for _target in self.browse(cr, uid, ids, context=context):
            res[_target.id] = True
            if _target.target_jumlah_kuantitas_output == 0 :
                res[_target.id] = False
            if _target.target_jumlah_kuantitas_output >0 :
                if _target.realisasi_lines:
                    for task_obj in _target.realisasi_lines:
                        if task_obj.target_jumlah_kuantitas_output == 0 and task_obj.state != 'done' :
                            res[_target.id] = False
                
        return res
    
    def _search_is_realisasi_ko_valid(self, cr, uid, obj, name, args, context=None):
        ids = set()
        
        
        for cond in args:
            filter_condition= cond[1]
            
            if filter_condition == '!=' :
                cr.execute("select project_id from project_skp where target_jumlah_kuantitas_output = 0 and state!='done' and active group by project_id")
            else :
               cr.execute("select project_id from project_skp where target_jumlah_kuantitas_output != 0  and state!='done' and active  group by project_id")
                
            res_ids = set(id[0] for id in cr.fetchall())
            ids = ids and (ids & res_ids) or res_ids
        if ids:
            return [('id', 'in', tuple(ids))]
        return [('id', '=', '0')]
    
    _columns = {
        'name': fields.char('Nama Kegiatan', size=500, select=True,required=True,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'code'     : fields.char('Kode Kegiatan',size=25,readonly=True,),
        'code_opd'     : fields.char('Kode DITJEN',size=2,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'code_program'     : fields.char('Kode Kegiatan',size=3,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'code_kegiatan'     : fields.char('Kode Kegiatan',size=2,readonly=True,
                            states={'draft': [('readonly', False)],'new': [('readonly', False)]},),
        'state': fields.selection([('draft','Draft'),('new','Baru'),
                                   ('propose','Penilaian Atasan'), ('rejected_manager', 'Penilaian Ditolak'),
                                   ('evaluated','Verifikasi BKD'), ('rejected_bkd', 'Pengajuan Ditolak Verifikatur'),
                                   ('confirm','Target Di Terima'), 
                                   ('pending','Pending'),
                                   ('propose_to_close','Pengajuan Closing Target'),('closed','Closed'),
                                   ('cancelled', 'Cancel'),
                                   ('propose_correction', 'Ajukan Perubahan Target'),
                                   ('correction', 'Revisi Target'),
                                   ], 'Status', required=True,),
        'lama_kegiatan'     : fields.integer('Lama Kegiatan',required=True,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'correction': [('readonly', False)]},),
        'notes'     : fields.text('Catatan'),
        'notes_atasan'     : fields.text('Catatan Dari Atasan'),
        'notes_bkd'     : fields.text('Catatan Dari BKD'),
        'satuan_lama_kegiatan'     : fields.selection([('bulan', 'Bulan')],'Satuan Waktu Lama Kegiatan',readonly=True),
        'target_type_id': fields.selection([          ('dpa_opd_biro', 'Kegiatan APBD - DPA'),
                                                      ('dipa_apbn', 'Kegiatan APBN - DIPA'),  
                                                      ('sotk', 'Kegiatan Tupoksi (Non DPA)'), 
                                                      ('lain_lain', 'Kegiatan Target Pendapatan Daerah'), 
                                                      ],
                                                      'Jenis Kegiatan', required=True, readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'correction': [('readonly', False)],'confirm': [('readonly', False)]},
                                                     ),
         'target_category_id': fields.selection([     ('program', 'Program'), 
                                                      ('kegiatan', 'Kegiatan'), 
                                                      ],
                                                      'Kategori',readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'correction': [('readonly', False)]},
                                                     ),
        'target_period_year'     : fields.char('Periode Tahun',size=4, required=True, readonly=True),
        'target_jumlah_kuantitas_output'     : fields.float('Kuantitas Output',required=True,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]} 
                                                            ,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_kuantitas_output'     : fields.many2one('satuan.hitung', 'Jenis Kuantitas Output' ,required=True,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]},),
        'target_angka_kredit'     : fields.float('Angka Kredit' ,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]}
                                                 ,digits_compute=dp.get_precision('angka_kredit')),
        'target_kualitas'     : fields.float('Kualitas',required=True,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]}
                                             ,digits_compute=dp.get_precision('no_digit')),
        'target_waktu'     : fields.integer('Waktu',required=True,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]}
                                            ,digits_compute=dp.get_precision('no_digit')),
        'target_satuan_waktu'     : fields.selection([('bulan', 'Bulan'),('hari', 'Hari')],'Satuan Waktu',select=1,required=True,readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]},),
        'target_biaya'     : fields.float('Biaya',readonly=True,states={'draft': [('readonly', False)],'new': [('readonly', False)],'propose': [('readonly', False)],'correction': [('readonly', False)]},),
        
        'status_target_bulanan': fields.selection([('belum', 'Target Bulanan Belum Dibuat'), ('sudah', 'Target Sudah Dibuat')], 'Status Target Bulanan', readonly=True),
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai',),
        'user_id_atasan': fields.many2one('res.users', 'Atasan Langsung', ),
        'user_id_banding': fields.many2one('res.users', 'Atasan Banding', ),
        'user_id_bkd': fields.many2one('res.users', 'Verifikatur', ),
        'realisasi_lines': fields.one2many('project.skp', 'project_id', 'Target Bulanan',order='target_period_month'),
        'total_biaya_bulanan': fields.function(_get_total_target_biaya_, method=True, readonly=True,string='Total Biaya Hasil Generate', store=False),
        'total_angka_kredit_bulanan': fields.function(_get_total_target_angka_kredit, method=True, readonly=True,string='Total Angka Kredit Hasil Generate', store=False
                                                      ,digits_compute=dp.get_precision('angka_kredit')),
        
        'count_correction'     : fields.integer('Jumlah Revisi',readonly=True),
        'count_evaluated'     : fields.integer('Jumlah Pengajuan Verifikasi',readonly=True),
        'date_start': fields.date('Tanggal Awal Kegiatan', select=True, copy=False),
        'date_end': fields.date('Tanggal Akhir Kegiatan', select=True, copy=False),
        'active': fields.boolean('Active', help="If the active field is set to False, it will allow you to hide the project without removing it."),
        'sequence': fields.integer('Urutan Kegiatan', help="Gives the sequence order when displaying a list of Projects."),
        
        'nilai_akhir': fields.float( 'Nilai',readonly=True),
        'jumlah_perhitungan': fields.float( 'Jumlah Perhitungan',readonly=True),
        'indeks_nilai_akhir': fields.selection([('a', 'A'), ('b', 'B'),('c', 'C'), ('d', 'D')], 'Indeks', readonly=True),
        
        'employee_id': fields.related('user_id', 'partner_id', relation='res.partner', type='many2one', string='Data Pegawai', store=True),
        'job_type': fields.related('employee_id', 'job_type',  type='char', string='Tipe Jabatan', store=True),
        'company_id': fields.many2one('res.company', 'DITJEN'),
        'currency_id': fields.many2one('res.currency', 'Currency'),
        
        'is_realisasi_ko_valid': fields.function(_is_realisasi_ko_valid, string='Realisasi Kuantitas Output Valid', type='boolean', fnct_search=_search_is_realisasi_ko_valid),
    }
    _defaults = {
        'active' :True,
        'target_period_year':lambda *args: time.strftime('%Y'),
        'user_id': lambda self, cr, uid, ctx: uid,
        'date_start':False,
        'satuan_lama_kegiatan':'bulan',
        'lama_kegiatan':12,
        'target_satuan_waktu':'bulan',
        'target_kualitas':100,
        'status_target_bulanan':'belum',
        'state': 'draft',
        'name':'',
        'target_jumlah_kuantitas_output':False,
        'target_angka_kredit':False,
        'target_kualitas':100,
        'target_waktu':12,
        'target_biaya':0,
        'count_correction':0,
        'count_evaluated':0,
        'currency_id':13,
        'company_id': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
        'code_opd:':lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.code,
    }
    
    def generate_target_realisasi_bulanan(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task =  {}
        
        target_pool = self.pool.get('project.project')
        task_pool = self.pool.get('project.skp')
        
        for target_obj in self.browse(cr, uid, ids, context=context):
            task_ids = task_pool.search(cr, uid, [('project_id','=',target_obj.id)], context=None)
            task_pool.set_cancelled(cr, uid, task_ids, context=None)
            
            
            description=target_obj.name
            lama_kegiatan=target_obj.lama_kegiatan
            user_id = target_obj.user_id.id
            target_period_month='xx'
            date_start='xx'
            date_end='xx'
            company_id=None
            currency_id=None
            user_id_bkd=None
            employee = target_obj.user_id.partner_id
            if user_id!=uid:
              raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            if not employee :
                raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Ada Beberapa Informasi Kepegawaian Belum Diisi, Khususnya Data Pejabat Penilai Dan Atasan Banding.'))
            else :
                company = employee.company_id
                company_id = company.id
                currency_id= employee.company_id and employee.company_id.currency_id and employee.company_id.currency_id.id
                
                if not company_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena DITJEN/Dinas Pegawai Belum Dilengkapi.'))
                
                if not target_obj.user_id_bkd:
                    if not company.user_id_bkd :
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                    _('Verifikatur Dari BKD Belum Diupdate,  Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else :
                        user_id_bkd = company.user_id_bkd.id
                else :
                    user_id_bkd=target_obj.user_id_bkd.id 
                if not employee.user_id_atasan :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Terisi.'))
                if not employee.user_id_atasan.user_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Melakukan Proses Aktifasi.'))
                if not employee.user_id_banding :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Terisi.'))
                
                if not employee.user_id_banding.user_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Melakukan Proses Aktifasi.'))
                    
                if not employee.job_type or not employee.job_id or not employee.golongan_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Jabatan Dan Pangkat Belum Diiisi.'))
            
            user_id_atasan =target_obj.user_id_atasan.id
            user_id_banding=target_obj.user_id_banding.id 
            
            if not target_obj.user_id_atasan.id :
                user_id_atasan = employee.user_id_atasan.user_id.id
            if not target_obj.user_id_banding.id :
                user_id_banding = employee.user_id_banding.user_id.id
            #init code:
            target_code = None
            if target_obj.code_opd and target_obj.code_program and target_obj.code_kegiatan:
                target_code = target_obj.code_opd +'.'+target_obj.code_program+'.'+target_obj.code_kegiatan
                
            
                
            task.update({
                           'project_id':target_obj.id,
                           'user_id':user_id,
                           'company_id':company_id,
                           'name': target_obj.name,
                           'code': target_code,
                           'code_opd':target_obj.code_opd,
                           'code_program':target_obj.code_program,
                           'code_kegiatan':target_obj.code_kegiatan,
                           'target_type_id':target_obj.target_type_id,
                           'target_category_id':target_obj.target_category_id,
                           'target_period_year': target_obj.target_period_year,
                           'target_jumlah_kuantitas_output'     : target_obj.target_jumlah_kuantitas_output,
                            'target_satuan_kuantitas_output'     : target_obj.target_satuan_kuantitas_output.id or None,
                            'target_angka_kredit'     : target_obj.target_angka_kredit,
                            'target_kualitas'     : target_obj.target_kualitas,
                            'target_waktu'     : target_obj.target_waktu,
                            'target_satuan_waktu'     : 'hari',
                            'target_biaya'     : target_obj.target_biaya,
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            'notes':'-',
                            'currency_id':currency_id,
                            
                            'active':True,
                           })
            #Update Task Target Bulanan
            now=DateTime.today();
            part_jumlah_kuantitas_output=0
            part_angka_kredit=0
            part_biaya=0
            kuantitas=target_obj.target_jumlah_kuantitas_output
            ang_kredit=target_obj.target_angka_kredit
            biaya=target_obj.target_biaya
            x_kuantitas=kuantitas/lama_kegiatan
            y_kuantitas =kuantitas%lama_kegiatan
            x_kredit=ang_kredit/lama_kegiatan
            y_kredit =ang_kredit%lama_kegiatan
            x_biaya=biaya/lama_kegiatan
            y_biaya =biaya%lama_kegiatan
            first_task_id=None
            sum_of_balance_biaya=0
            if target_obj.date_start :
                curr_date = DateTime.strptime(target_obj.date_start,'%Y-%m-%d')
            else :
                january=DateTime.Date(now.year,1,1)
                curr_date =  DateTime.strptime(january.strftime('%Y-%m-%d'),'%Y-%m-%d')
            first_date =curr_date
            #print "THIS IS A DATE ",curr_date
            for i in range(0,lama_kegiatan):
                #Jumlah Kuantitas Output
                if kuantitas >0 :
                    if i < y_kuantitas :
                        part_jumlah_kuantitas_output=x_kuantitas+1
                    else :
                        part_jumlah_kuantitas_output=x_kuantitas
                #angka Kredit
                #if ang_kredit >0 :
                #    if i < y_kredit :
                #        part_angka_kredit=x_kredit+1
                #    else :
                part_angka_kredit=x_kredit   
                #Biaya
                if biaya >0 :
                    part_biaya=round(x_biaya) 
                    sum_of_balance_biaya+=part_biaya    
                next_date = curr_date + DateTime.RelativeDateTime(months=i)
                target_period_month=next_date.strftime('%m')
                task.update({
                           'target_period_month':target_period_month,
                           'target_jumlah_kuantitas_output'     : part_jumlah_kuantitas_output,
                           'target_waktu'     : 20,
                           'target_angka_kredit'     : part_angka_kredit,
                           'target_biaya'     : part_biaya,
                 })
                date_start='xx'
                date_end='xx'
                state='draft';
                task.update({ 'state':state,
                            })
                if i == (lama_kegiatan-1) :
                    balancing_biaya = target_obj.target_biaya - sum_of_balance_biaya
                    task.update({
                                'target_biaya':balancing_biaya+part_biaya
                        })
                   #task_pool.write(cr, uid, task_id,update_biaya)

                #insert task
                
                task_id = task_pool.create(cr, uid, task,context)
                
                
            
            #Update Status Target Bulanan
            update_target = {
                            'status_target_bulanan':'sudah',
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            #'date_start':first_date or False
                        }
            
            self.write(cr,uid,target_obj.id,update_target,context)
            
   
            return True;
    def generate_revisi_target_realisasi_bulanan(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task =  {}
        member =  {}
        
        target_pool = self.pool.get('project.project')
        task_pool = self.pool.get('project.skp')
        
        for target_obj in self.browse(cr, uid, ids, context=context):
            task_ids = task_pool.search(cr, uid, [('project_id','=',target_obj.id),('state','in',('draft','realisasi','cancelled'))], context=None)
            task_pool.set_cancelled(cr, uid, task_ids, context=None)
            task_exist_ids = task_pool.search(cr, uid, [('project_id','=',target_obj.id),('state','not in',('draft','realisasi','cancelled'))], context=None)
            
            
            description=target_obj.name
            lama_kegiatan=target_obj.lama_kegiatan
            user_id = target_obj.user_id.id
            target_period_month='xx'
            date_start='xx'
            date_end='xx'
            company_id=None
            currency_id=None
            user_id_bkd=None
            employee = target_obj.user_id.partner_id
            if user_id!=uid:
              raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            if not employee :
                raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Ada Beberapa Informasi Kepegawaian Belum Diisi, Khususnya Data Pejabat Penilai Dan Atasan Banding.'))
            else :
                company = employee.company_id
                company_id = company.id
                currency_id= employee.company_id and employee.company_id.currency_id and employee.company_id.currency_id.id
                
                if not company_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena DITJEN/Dinas Pegawai Belum Dilengkapi.'))
                
                if not target_obj.user_id_bkd:
                    if not company.user_id_bkd :
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                    _('Verifikatur Dari BKD Belum Diupdate,  Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else :
                        user_id_bkd = company.user_id_bkd.id
                else :
                    user_id_bkd=target_obj.user_id_bkd.id 
                if not employee.user_id_atasan :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Terisi.'))
                if not employee.user_id_atasan.user_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Langsung (Pejabat Penilai) Belum Melakukan Proses Aktifasi.'))
                if not employee.user_id_banding :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Terisi.'))
                if not employee.user_id_banding.user_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Atasan Banding (Pejabat Pengajuan Banding) Belum Melakukan Proses Aktifasi.'))
            
            user_id_atasan =target_obj.user_id_atasan.id
            user_id_banding=target_obj.user_id_banding.id 
            
            if not target_obj.user_id_atasan.id :
                user_id_atasan = employee.user_id_atasan.user_id.id
            if not target_obj.user_id_banding.id :
                user_id_banding = employee.user_id_banding.user_id.id
            #init code:
            target_code = None
            if target_obj.code_opd and target_obj.code_program and target_obj.code_kegiatan:
                target_code = target_obj.code_opd +'.'+target_obj.code_program+'.'+target_obj.code_kegiatan
            task.update({
                           'project_id':target_obj.id,
                           'user_id':user_id,
                           'company_id':company_id,
                            'name': target_obj.name,
                           
                           'code': target_obj.code,
                           'code': target_code,
                           'code_opd':target_obj.code_opd,
                           'code_program':target_obj.code_program,
                           'code_kegiatan':target_obj.code_kegiatan,
                           
                           'target_type_id':target_obj.target_type_id,
                           'target_category_id':target_obj.target_category_id,
                           'target_period_year': target_obj.target_period_year,
                           'target_jumlah_kuantitas_output'     : target_obj.target_jumlah_kuantitas_output,
                            'target_satuan_kuantitas_output'     : target_obj.target_satuan_kuantitas_output.id or None,
                            'target_angka_kredit'     : target_obj.target_angka_kredit,
                            'target_kualitas'     : target_obj.target_kualitas,
                            'target_waktu'     : target_obj.target_waktu,
                            'target_satuan_waktu'     : 'hari',
                            'target_biaya'     : target_obj.target_biaya,
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            'notes':'-',
                            'currency_id':currency_id,
                            
                            'active':True,
                           })
            #Update Task Target Bulanan
            old_kualitas,old_biaya,old_ak = self.get_total_target_aspect_task(cr,uid,task_exist_ids,context=None)
            now=DateTime.today();
            part_jumlah_kuantitas_output=0
            part_angka_kredit=0
            part_biaya=0
            kuantitas=target_obj.target_jumlah_kuantitas_output - old_kualitas
            ang_kredit=target_obj.target_angka_kredit - old_ak
            biaya=target_obj.target_biaya - old_biaya
            part_lama_kegiatan = lama_kegiatan - len(task_exist_ids)
            x_kuantitas=kuantitas/part_lama_kegiatan
            y_kuantitas =kuantitas%part_lama_kegiatan
            x_kredit=ang_kredit/part_lama_kegiatan
            y_kredit =ang_kredit%part_lama_kegiatan
            x_biaya=biaya/part_lama_kegiatan
            y_biaya =biaya%part_lama_kegiatan
            first_task_id=None
            sum_of_balance_biaya=0
            if target_obj.date_start :
                curr_date = DateTime.strptime(target_obj.date_start,'%Y-%m-%d')
            else :
                january=DateTime.Date(now.year,1,1)
                curr_date =  DateTime.strptime(january.strftime('%Y-%m-%d'),'%Y-%m-%d')
            first_date =curr_date
            
            for i in range(0,lama_kegiatan):
                next_date = curr_date + DateTime.RelativeDateTime(months=i)
                target_period_month=next_date.strftime('%m')
                
                if self.is_exist_task_in_month(cr,uid,task_exist_ids,target_period_month,target_obj.target_period_year,context=None):
                    
                    continue;
                
                if kuantitas >0 :
                    if i < y_kuantitas :
                        part_jumlah_kuantitas_output=x_kuantitas+1
                    else :
                        part_jumlah_kuantitas_output=x_kuantitas
                part_angka_kredit=x_kredit   
                
                #Biaya
                if biaya >0 :
                    part_biaya=round(x_biaya) 
                    sum_of_balance_biaya+=part_biaya    
                
                task.update({
                           'target_period_month':target_period_month,
                           'target_jumlah_kuantitas_output'     : part_jumlah_kuantitas_output,
                           'target_waktu'     : 20,
                           'target_angka_kredit'     : part_angka_kredit,
                           'target_biaya'     : part_biaya,
                 })
                date_start='xx'
                date_end='xx'
                state='draft';
                task.update({
                                 'state':state,
                        })
                if i == (lama_kegiatan-1) :
                    balancing_biaya = biaya - sum_of_balance_biaya
                    task.update({
                                'target_biaya':balancing_biaya+part_biaya
                        })
                   #task_pool.write(cr, uid, task_id,update_biaya)

                #insert task
                
                task_id = task_pool.create(cr, uid, task)
                
                # Update Realisasi Yang sudah selesai Dan inprogress
            exist_task_data = {}
            for task_exist_obj in task_pool.browse(cr, uid, task_exist_ids, context=context):
                    exist_task_data.update({
                           'project_id':target_obj.id,
                           'name': target_obj.name,
                           'code': target_obj.code,
                           
                           'target_type_id':target_obj.target_type_id,
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            
                      })
                    task_pool.write(cr,uid,task_exist_obj.id,exist_task_data)
            #Update Status Target Bulanan
            update_target = {
                            'status_target_bulanan':'sudah',
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            'date_start':first_date or False
                        }
            
            target_pool.write(cr,uid,target_obj.id,update_target)
            
        return True
    def is_exist_task_in_month(self, cr, uid,exist_task_ids, period_month, period_year,context=None):
        if exist_task_ids : 
            for task_obj in self.pool.get('project.skp').browse(cr, uid,exist_task_ids, context=context):
                if task_obj.target_period_month == period_month and task_obj.target_period_year == period_year :
                    return True
        return False  
    def get_total_target_aspect_task(self, cr,uid, exist_task_ids,context=None):
        total_kuantitas=total_biaya=total_ak =0.0
        if exist_task_ids : 
            for task_obj in self.pool.get('project.skp').browse(cr, uid,exist_task_ids, context=context):
                total_kuantitas +=task_obj.target_jumlah_kuantitas_output
                total_biaya +=task_obj.target_biaya
                total_ak +=task_obj.target_angka_kredit
                
        return total_kuantitas,total_biaya,total_ak      
    
    def get_auth_id(self, cr, uid, ids,type, context=None):
        if not isinstance(ids, list): ids = [ids]
        #print "CONTEXT : ", context
        by_pass_auth=False
        if context:
            by_pass_auth = context.get('bypass_auth',False)
        if by_pass_auth:
            return True;
        if uid == 1:
            return True;
        for target in self.browse(cr, uid, ids, context=context):
            
            if type=='user_id' :
                if target.user_id :
                    if target.user_id.id!=uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type=='user_id_atasan' :
                if target.user_id_atasan :
                    if target.user_id_atasan.id!=uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            elif type=='user_id_bkd' :
                if target.user_id_bkd :
                    if target.user_id_bkd.id!=uid :
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
                
            else : 
               return False;
            
        return True;
    def is_get_auth_id(self, cr, uid, ids,type, context=None):
        if not isinstance(ids, list): ids = [ids]
        #print "CONTEXT : ", context
        by_pass_auth=False
        if context:
            by_pass_auth = context.get('bypass_auth',False)
        if by_pass_auth:
            return True;
        if uid == 1:
            return True;
        for target in self.browse(cr, uid, ids, context=context):
            
            if type=='user_id' :
                if target.user_id :
                    if target.user_id.id!=uid :
                        return False
            elif type=='user_id_atasan' :
                if target.user_id_atasan :
                    return False
            elif type=='user_id_bkd' :
                if target.user_id_bkd :
                    if target.user_id_bkd.id!=uid :
                        return False
                
            else : 
               return False;
            
        return True;
    #
    def is_user(self,cr,uid,ids,context=None):
        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_auth_id(cr, uid, [target_id],'user_id', context=context) 
    def is_manager(self,cr,uid,ids,context=None):
        
        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_auth_id(cr, uid, [target_id],'user_id_atasan', context=context) 
    def is_verificator(self,cr,uid,ids,context=None):
        target_id = len(ids) and ids[0] or False
        if not target_id: return False
        return self.get_auth_id(cr, uid, [target_id],'user_id_bkd', context=context)
    
    def is_can_back_to_user(self, cr, uid, ids, context=None):
        for target_obj in self.browse(cr, uid, ids, context=context):
                if target_obj.state == 'propose' :
                    if target_obj.count_evaluated > 0:
                        raise osv.except_osv(_('Invalid Action!'),
                                             _('Kegiatan Ini Sudah Pernah Melalui Proses Verifikasi, Tidak Dapat Dikembalikan Ke Pegawai.'))
                    else :
                        return True
                else :
                    return True;
        return True;
    def set_draft(self,cr,uid,ids,context=None):
        return self.write(cr, uid, ids, {'state':'draft'}, context=context) 
    def set_new(self,cr,uid,ids,context=None):
        if self.is_can_back_to_user(cr, uid, ids, context) :
            return self.write(cr, uid, ids, {'state':'new'}, context=context)
         
    def set_propose(self, cr, uid, ids, context=None):
        for target in self.browse(cr, uid, ids, context=context):
                #print "Target Bulanan ",target.status_target_bulanan
                if target.status_target_bulanan!='sudah' :
                    raise osv.except_osv(_('Invalid Action!'),
                                             _('Sebelum Pengajuan, Harap Generate Target Bulanan Terlebih Dahulu.'))
                if target.target_biaya >0:
                    if target.target_biaya != target.total_biaya_bulanan:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Penyusunan Target Biaya Bulanan Tidak Sama Dengan Target Biaya Tahunan'))
                if target.target_angka_kredit >0:
                    if target.target_angka_kredit != target.total_angka_kredit_bulanan:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Penyusunan Target Total Angka Kredit Tidak Sama Dengan Hasil Generate'))
                if target.lama_kegiatan != target.target_waktu:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Penyusunan Waktu Antara Lama Kegiatan Dan Target Waktu Harus Sama'))
                if not target.is_realisasi_ko_valid:
                        raise osv.except_osv(_('Invalid Action!'),
                                                 _('Penyusunan Realisasi Kuantitas Output Tidak Boleh Ada Yang 0'))
        return self.write(cr, uid, ids, {'state':'propose'}, context=context)
    def set_propose_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'new'}, context=context)
    def set_evaluated(self, cr, uid, ids, context=None):
        self.counting_evaluated(cr, uid, ids, context)
        return self.write(cr, uid, ids, {'state':'evaluated'}, context=context)
    def counting_evaluated(self, cr, uid, ids, context=None):
        for target_obj in self.browse(cr, uid, ids, context=context):
                x_count_evaluated = 1 + target_obj.count_evaluated 
                self.write(cr, uid, [target_obj.id,], {'count_evaluated':x_count_evaluated})
        
    def set_evaluate_rejected(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose'}, context=context)
    def set_confirm(self, cr, uid, ids, context=None):
        task_pool = self.pool.get('project.skp')
        task_ids = task_pool.search(cr, uid, [('project_id','in',ids),('state','in',('draft','realisasi','cancelled'))], context=None)
        if task_ids:
            task_pool.fill_skp_task_automatically_with_target(cr,uid,task_ids,context=None)
            task_pool.set_realisasi(cr,uid,task_ids,context=None)
            task_pool.write(cr,uid,task_ids,{'active':True},context=None)
            
            #for task_id in task_ids:
            #    workflow.trg_validate(uid, 'project.skp', task_id, 'action_relisasi', cr)
                
            #task_pool.signal_workflow(cr, uid, task_ids, 'action_realisasi')
        return self.write(cr, uid, ids, {'state':'confirm'}, context=context)
    def cancel_target(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task_pool = self.pool.get('project.skp')
        for target_id in ids:
            task_ids = task_pool.search(cr, uid, [('project_id','=',target_id)], context=None)
            task_pool.set_cancelled(cr, uid, task_ids, context=None)
            self.write(cr, uid, [target_id], {'state':'cancelled'}, context=context)
        return True;
       
    def set_correction_reject(self, cr, uid, ids, context=None):
            return self.write(cr, uid, ids, {'state':'confirm'}, context=context)
    def counting_correction(self, cr, uid, ids, context=None):
        for target_obj in self.browse(cr, uid, ids, context=context):
                    x_count_correction = target_obj.count_correction + 1
                    self.write(cr, uid, [target_obj.id,], {'count_correction':x_count_correction}, context=context)
        return True
    def approve_correction(self, cr, uid, ids, context=None):
        wf_service = netsvc.LocalService("workflow")
        self.counting_correction(cr, uid, ids, context)
        for id in ids :
            wf_service.trg_validate(uid, 'project.project', id, 'action_correction_approve', cr)
        
    def set_propose_correction(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose_correction'}, context=context)
    def set_propose_closing(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'propose_to_close'}, context=context)
    def set_cancelling_closed_target(self, cr, uid, ids, context=None):
        user_id_bkd = uid
        for target_obj in self.browse(cr,uid,ids,context=None) :
            if target_obj.user_id_bkd :
                user_id_bkd = target_obj.user_id_bkd.id
                break
        return self.cancel_target(cr, user_id_bkd, ids, context=context)
    def set_closing_reject(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state':'confirm'}, context=context)
    def set_closing_approve(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task_pool = self.pool.get('project.skp')
        for target_id in ids :
                task_ids = task_pool.search(cr, uid, [('project_id','=',target_id),('state','in',('draft','realisasi'))], context=None)
                task_pool.set_closed(cr, uid, task_ids,  context=None)
        self.write(cr, uid, ids, {'state':'closed'}, context=context)
        return True
    
    
project()    
