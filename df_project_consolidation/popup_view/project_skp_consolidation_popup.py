from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime
import openerp.addons.decimal_precision as dp

# ====================== Popup Class Object ================================= #

#class project_skp_propose_review_byatasan(osv.Model):
    #_name = 'project.skp.propose.review.byatasan'
    #_description = 'Realisasi Perilaku , Pengajuan Review Kegiatan'
    #_columns = {
        #'review_notes'                : fields.text('Catatan Review', required=True ),
        #}
#project_skp_propose_review_byatasan()   
class project_skp_propose_review(osv.Model):
    _name = 'project.skp.propose.review'
    _description = 'Realisasi SKP , Pengajuan Review Kegiatan'
    _columns = {
        'review_notes'                : fields.text('Catatan Review', required=True ),
        'review_realiasasi_jumlah_kuantitas_output'     : fields.integer('Usulan Kuantitas Ouput', ),
        'review_realiasasi_kualitas'     : fields.float('Usulan Kualitas', digits_compute=dp.get_precision('no_digit')),
        'review_realiasasi_waktu'     : fields.integer('Usulan Waktu', ),
        'review_realiasasi_angka_kredit'     : fields.float('Usulan Angka Kredit',digits_compute=dp.get_precision('angka_kredit')),
        'review_realiasasi_biaya'     : fields.float('Usulan Biaya',digits_compute=dp.get_precision('no_digit')),
        'nilai_sebelum_review'     : fields.float('Nilai Akhir Sebelum Review', ),
        'task_id'     : fields.many2one('project.skp', 'Realisasi', readonly=True),
         
        'is_review'                : fields.boolean('Direview', ),
        'is_dpa_review'                : fields.boolean('Review DPA', ),
        'hide_by_target_type_id'                : fields.boolean('Kegiatan DPA/APBD?', ),
        
       
    }
    def action_propose_dpa_review(self, cr, uid, ids, context=None):
        
        vals = {}
        task_pool = self.pool.get('project.skp')
        review_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({               'is_review' : True,
                                    'is_dpa_review':True,
                                    'review_notes' : review_obj.review_notes,
                                    'review_realiasasi_biaya':review_obj.review_realiasasi_biaya,
                                    'nilai_sebelum_review':review_obj.nilai_sebelum_review,
                                    'state':'propose_to_review',
                                     })
        task_pool.write(cr, review_obj.task_id.user_id_bkd.id, [review_obj.task_id.id], vals, context)
        
        return True;
    def action_propose_review_by_atasan(self, cr, uid, ids, context=None):
        
        vals = {}
        task_pool = self.pool.get('project.skp')
        review_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({               'is_review' : True,
                                    'is_dpa_review':False,
                                    'review_notes' : review_obj.review_notes,
                                    'review_realiasasi_jumlah_kuantitas_output':review_obj.review_realiasasi_jumlah_kuantitas_output,
                                    'review_realiasasi_kualitas':review_obj.review_realiasasi_kualitas,
                                    'review_realiasasi_waktu':review_obj.review_realiasasi_waktu,
                                    'review_realiasasi_biaya':review_obj.review_realiasasi_biaya,
                                    'review_realiasasi_angka_kredit':review_obj.review_realiasasi_angka_kredit,
                                    'nilai_sebelum_review':review_obj.nilai_sebelum_review,
                                    'state':'propose_to_review',
                                     })
        task_pool.write(cr, review_obj.task_id.user_id_bkd.id, [review_obj.task_id.id], vals, context)
    
project_skp_propose_review()
